---
sidebar_position: 4
description: aus den Beschlüssen abgeleitete Regelungen für IT-Sicherheit 
---

# Rahmenwerk zur Bewertung der (Basis-)Dienste und Produkte
Aus den Anforderungen aus den Gesetzen, den Softwaretechnik-Aspekten, sowie den Datenschutz-Bestimmungen im Rahmen des OZG-Kontextes wurden Regelungen abgeleitet, die im Folgenden als Rahmenwerk zusammengefasst wurden und die Grundlage für die Bewertung von ausgewählten (Basis-)Diensten und Produkten dienen soll:

| Allgemeines |
| --- |
|Wurde das Produkt bereits zertifiziert bzw. ein Audit durchgeführt? *z.B. Common Criteria, ISO, IT-Grundschutz, technische Richtlinien, Pentests etc.* | 
| In welchen Intervallen werden Audits durchgeführt?|
|Verfügt das Produkt über Onlineschnittstellen, durch die Nutzern Zugriff auf Dienste im E-Government-Bereich angeboten werden? Falls ja, welche URLs? |
| Verfügt das Produkt über eine Benachrichtigungskomponente? *Falls ja: Auf welche Art werden Benutzer benachrichtigt?* |
| Gibt es einen IT-Sicherheitsbeauftragten für das Produkt? |
| Kann der Nutzer das Produkt mit Standard-Hardware benutzen oder muss ein zusätzliches Gerät angeschafft werden? *z.B. NFC-Lesegerät, 2FA-Gerät etc.* |
| Gibt es eine Beziehung zu anderen (Basis-)Diensten? *Falls ja, welche?* |
| Erfolgt die Übermittlung der Nutzerkennung an andere (Basis-)Dienste? *z.B. über SAML* |
| Gibt es Informationen zu Responsible Disclosure? |

| Authentisierung (nach TR-03107-1 Elektronische Identitäten und Vertrauensdienste im E-Government Teil 1) |
| --- |
| Welche Vertrauensniveaus werden angeboten? *3 Vertrauensniveaus: normal, substantiell, hoch (gemäß IT-Grundschutz BSI100-2), gemessen an dem Widerstand eines Angreifers mit entsprechendem Angriffspotential, definiert in ISO18045* |
| Welche Protokolle kommen für die Authentisierung zum Einsatz? *z.B. TLS, SAML, Kerberos etc.* |
| Welche Authentisierungsmittel kommen zum Einsatz? *z.B. Besitz, Wissen, Biometrie, Multi-Faktor-Au., eID, Softwaretoken, OTP, smsTAN* |

| Kryptographie (nach BSI TR-03116) |
|---|
|Kommt TLS für die Kommunikation zum Einsatz? Welche Version? (BSI-TR-03116-4) *Verpflichtend ist mindestens TLS 1.2, optional ist TLS 1.3, nicht erlaubt sind TLS 1.1, TLS 1.0, SSL 3, SSL 2 (siehe TLS Checkliste)* |
| Gibt es eine Ende-zu-Ende-Verschlüsselung? *Falls nicht: An welchen Stellen wird davon abgewichen?* |
| Welche kryptographischen Verfahren werden eingesetzt, um die Vorwärtssicherheit zu gewährleisten? |
| Ist die sichere Speicherung privater Schlüssel gewährleistet? (Geschützt gegen unberechtigtes Kopieren und Verwenden) |
| Ist die sichere Speicherung öffentlicher Schlüssel gewährleistet? (Geschützt gegen Manipulation) |
| Werden Nachrichten digital signiert? |
|Ist die Überprüfung der Zertifikate jedem Nutzer möglich oder bspw. nur Behörden vorbehalten? |
| Welche Zertifikate werden eingesetzt? Kommt eine Public-Key-Infrastruktur zum Einsatz? |
| Sind die Vorgaben für die minimale Schlüssellänge eingehalten? *siehe auch https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-4.pdf* |

Optional:
Der Schlüssel im Server-Zertifikat entspricht den kryptographischen Mindestanforderungen:
 - RSA-Schlüssel:
    - Mindestens 2048 Bitlänge
 - ECDSA-Schlüssel:
    - Es wird eine der folgenden Kurven verwendet:
       - brainpoolP256r1
       - brainpoolP384r1
       - brainpoolP512r1
       - secp256r1
       - secp384r1
       - secp521r1

Der Signaturalgorithmus des Server-Zertifikats entspricht den Anforderungen:
 - Signaturalgorithmus:
    - RSA
    - ECDSA
 - Hashfunktion:
    - SHA-256
    - SHA-384
    - SHA-512

| Datenschutz |
|--- |
| Wird eine Datenschutzerklärung bereitgestellt? |
| Gibt es einen Datenschutzbeauftragten? |
| Sind die Grundsätze des Datenschutzes (aus der SDG Verordnung) eingehalten? *genannt sind der Grundsatz der Datenminimierung, der Richtigkeit, der Speicherbegrenzung, der Integrität und Vertraulichkeit, der Notwendigkeit, der Verhältnismäßigkeit und der Zweckbindung* |

| Implementierung/SW-Aspekte |
| --- |
|Ist die Architektur komponentenhaft? |
| Welche Abhängigkeiten zu Bibliotheken, Drittanbieterdiensten sind vorhanden (sofern bekannt)? |

Für die Bewertung sind nicht alle Kriterien entscheidend. Einzelnen Dienste oder Produkte haben evtl. keine Authentisierungskomponente falls sie eine entsprechende Beziehung zu einem Authentisierungsdienst haben; dann ist dies entsprechend zu kennzeichnen. Zudem soll die Bewertung Aufschluss über Beachtenswertes beim Einsatz geben. Sie soll keinen Vergleich ermöglichen, da selten eine Auswahlmöglichkeit bzw. ein Wettbewerb existiert.
