---
sidebar_position: 2.5
description: Beschreibung der verschiedenen Ebenen des OOTS
---

# Once-Only Technical System

## National
Beim Nationalen OOTS (kurz NOOTS) soll eine G2B, G2C und G2G Datenübertragung innerhalb von Deutschland zur Verfügung gestellt werden. Dabei sollen Kunden ihre angeforderten Daten als Maske innerhalb der Formulare eingetragen sehen. Dies hat einerseits den Zweck, dass sie diese überprüfen und bei Bedarf abändern können und andererseits, um Transparenz über die übertragenen Daten zu erzeugen. 
Das NOOTS soll Daten zwischen Data-Consumer und Data-Producer austauschen. Es basiert, wie das EU-OOTS, auf dem [4-Corner-Modell](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiWw_Te8b2AAxUPrKQKHTVFCqkQFnoECBgQAQ&url=https%3A%2F%2Fwww.oeffentliche-it.de%2Fdocuments%2F10181%2F188095%2FTechnische%2BPerspektiven%2Bder%2BRegistermodernisierung.pdf%2Fe49d4846-826e-db70-fc46-cf3abafd66ad%3Ft%3D1614960958853%26download%3Dtrue&usg=AOvVaw3JGP8XPWox6VYWDVangKyG&opi=89978449). Während jedoch das EU-OOTS auf dem AS4-Protokoll beruhen soll, soll das NOOTS auf OSCI-Basis umgesetzt werden [@bva:oots].

## EU
Das EU-OOTS beschränkt sich auf G2C und G2B. Die Nachweise werden zudem als Preview dargestellt, sofern die Übertragung vom Nutzer initialisiert wurde. Wie bereits erwähnt, wird hier das 4-Corner-Modell genutzt. Dabei soll allerdings das AS4 Protokoll-genutzt werden. Somit sollen für beide OOTS unterschiedliche Protokolle genutzt werden. Dies macht die Nutzung einer intermediären Plattform notwedig.[@bva:oots]


## Kopplung von NOOTS und EU-OOTS

Durch die Nutzung eines anderen Protokolls in NOOTS wird der Einsatz einer intermediären Plattform notwendig. Diese wird zwischen EU-OOTS und NOOTS platziert und übersetzt Nachrichten aus dem AS4-Protokoll in das OSCI Protokoll und umgekehrt. Der Vorteil dieses Prinzips ist, dass das NOOTS vom EU-OOTS vollständig entkoppelt wird und Deutschland damit befähigt wird, ein eigenes OOTS mit eigenen Features zu nutzen, ohne dass das EU-OOTS für Deutschland explizit ausgestaltet werden muss. Durch die [SDG-VO](/docs/grundlagen-und-rahmen/sdg) können intermediäre Plattformen für diese vorgelagerte Komunikation genutzt werden. Somit müssen Evidence Provider und Requester nicht gleichzeitig an zwei Systeme angeschlossen werden. Ebenfalls ist das Management der Interoperabilität hier an einer Zentralen Stelle zu finden [@bva:oots].

## Weiterführende Links
- [What ist OOTS (Europäische Kommission)](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/About+OOTS)
- [NOOTS (OZG.de)](https://www.onlinezugangsgesetz.de/SharedDocs/downloads/Webs/OZG/DE/NOOTS-technical-design-documents-1-3.pdf?__blob=publicationFile&v=3)

## Quellen 
