---
sidebar_position: 2
description: Beschreibung und Einordnung SDG-VO
---

# SDG-VO

Die [Single-Digital-Gateway Verordnung (2018/1724)](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32018R1724) wurde 2018 vom Europäischen Parlament beschlossen. Ziel der EU-Verordnung ist es, einen einheitlichen elektronischen Zugang zu Informationen, digitalen Verwaltungsverfahren und zu Hilfsdiensten zu gewährleisten. Diese Angebote sollen in einem Netzwerk aus dem europäischen Portal und nationalen, Länder- und kommunalen Portalen zugänglich sein.

### Hintergrund

Um die Verwaltungsdigitalisierung in den Mitgliedstaaten weiter vorzutreiben, beschließt die Europäische Union 2018 die Single-Digital-Gateway-Verordnung (2018/1724). Diese kann im Wesentlichen als eine Weiterentwicklung der 2006 in Kraft getretenen Europäischen Dienstleistungsrichtlinie (2006/123/EG) betrachtet werden. Mit Hilfe dieser Richtlinie sollte ein europäischer Binnenmarkt auch für den Dienstleistungssektor geschaffen werden [@Klein_SGD]. Ziel war die Einrichtung einheitlicher Ansprechpartner:innen in den jeweiligen Mitgliedstaaten einzurichten und eine elektronische, standortunabhängige Verfahrensabwicklung anzubieten. Außerdem sollten die Verfahrensgebühren sowie diskriminierungsfreie Handelsgrundlagen hinsichtlich Firmensitzes, Staatsangehörigkeit oder Kapitalwert angepasst werden [@Klein_SGD]. „Dahinter steht der Gedanke des sog. One-Stop Governments, wonach Bürger oder Unternehmen unabhängig von örtlichen und sachlichen Zuständigkeiten die Möglichkeit der Erledigung aller Verwaltungsangelegenheiten bei einer Anlaufstelle bekommen sollen.“ [@Voss_sdg]. Im Idealfall wenden sich Dienstleister an die einheitlichen Ansprechpartner:innen, die die Anfragen dann an die jeweilige Behörde weiterleiten, so dass die Entscheidung weiterhin bei den zuständigen Stellen in den Mitgliedstaaten getroffen wird.

### Umsetzungsproblematik EU-DLR

Eine von der EU-Kommission in Auftrag gegebene Studie von 2015 zeigt, dass die Umsetzung der Richtlinie nur einen mäßigen Erfolg hatte. Deutschland belegte darin gar den letzten Platz. Die Gründe für das Scheitern, so Voss, lauten wie folgt:
* „Mangelnde ebenen- und ressortübergreifende Koordination und Kooperation
* Zeitraubende Diskussionen und schwache Informationspolitik
* Schwierigkeiten bei der Einführung elektronischer Verfahren und fehlende Abstimmung
* Keine bedarfsgerechten Prozesse
* Marketing und Nutzerorientierung unzureichend“ [@Voss_sdg] (Kapitelüberschriften).

Besonders im bundesdeutschen Kontext lässt sich das Scheitern besonders auf die Vernachlässigung basaler verwaltungsorganisatorischer wie -kultureller Faktoren zurückführen [@Schuppan_SDG]. Dazu gehören u.a. ein nur schwer zu überwindendes Silodenken hinsichtlich der Verortung des/der einheitlichen Ansprechpartner:in (vorwiegend auf Landesebene) und die Auswahl der Serviceangebote. Dadurch wird eine Harmonisierung der genannten Bereiche verhindert [@Schuppan_SDG]. Um dieses Inseldenken, hin zu Kooperation und Vernetzung, zu überwinden, braucht es Kompetenzentwicklung und Reflexion der Interaktions- wie Kooperationsformen für funktionierende Verfahren [@Schuppan_SDG].
Zusammengefasst fehlte für eine erfolgreiche Umsetzung der EU-DLR ein umfassendes arbeitsorganisatorisches und -kulturelles Changemanagement [@Schuppan_SDG].

### Ziel der SDG-VO

Die SDG-VO hat einige der Kritikpunkte der EU-DLR aufgenommen. Das Ziel der Verordnung ist es, einen einheitlichen, elektronischen Zugang zu Informationen, digitalen Verwaltungsverfahren sowie zu „Hilfs- und Problemlösungsdiensten“ bereitzustellen [@Voss_sdg]. Diese Angebote sollen in das bereits vorhandene Portal [YourEurope](https://europa.eu/youreurope/) integriert werden.

Mit der SDG-VO soll schließlich „ein Netzwerk aus Kommunal- und Länderportalen, nationalen Portalen und dem europäischen Portal“ entstehen [@BVA_YE]. Seckelmann spricht hierbei von einem Metaportalverbund. Dies meint ein von der EU-Kommission verwaltetes Zugangstor zu den jeweiligen Websites der Europäischen Union und der einzelnen Mitgliedstaaten [@BVA_YE] [@Seckelmann_sdg].

### Inhalt der SDG-VO und Kriterien für die Umsetzung

Die in der SDG-VO abgedeckten Verwaltungsverfahren müssen dabei mindestens einen „potenziell grenzüberschreitenden Bezug nachweisen“ sowie wenigstens in analoger Form in mehreren Mitgliedstaaten vorhanden bzw. eingerichtet sein [@Voss_sdg]. Diese sind zudem zunächst auf die folgenden Bereiche beschränkt:
 
![Rupp 2021](https://www.vdz.org/sites/default/files/styles/d12/public/6626/2021/bild2_verfahren_nach_artikel_6.png?itok=m4Y6ymf4)
[@Rupp_SDG]

Alternativ spricht Wimmer von insgesamt 15 Verfahren für Bürger:innen und sechs zusätzlichen für Unternehmen [@Wimmer_OO]. Die angestrebte vollständig digitale Abwicklung der genannten Verfahren müssen laut SDG-VO die folgenden Kriterien erfüllen:

- Eine nutzer:innenfreundliche und strukturierte digitale Durchführung der „die Identifzierung der Nutzerinnen und Nutzer, die Bereitstellung von Informationen und Nachweisen sowie die Signatur“ [@Wimmer_OO]
- Das Senden einer automatischen Empfangsbestätigung nach Einreichen des Verfahrens
- Eine digitale Übermittlung des Ergebnisses (sofern mit den geltenden Vorschriften vereinbar)
- Die Zustellung einer elektronischen Benachrichtigung über den Abschluss des Verfahrens [@Wimmer_OO]

Das Ziel der Vernetzung baut auf die technische Komponente gemäß des Once-Only-Prinzips auf [@Wimmer_OO]. Voraussetzung dafür ist eine Integration aller notwendigen Onlinedienste, darunter eine eID-Lösung (eIDAS-konform) zur Identifizierung oder elektronische Zahlungsmethoden, sowie die Einbindung in die European Interoperability Reference Architecture [@Rupp_SDG] [@Wimmer_OO] (für weitere Informationen zur EIRA siehe https://joinup.ec.europa.eu).
Bis zum 12.12.2023 soll das Vorhaben vollständig umgesetzt werden, so dass die wichtigsten Verfahrensbündel grenzüberschreitend und vollständig digital bereitstehen, wobei die Frist für den Informationszugang über allgemeine Rechte auf den 12.12.2020 und über angebotene Leistungen auf den 12.12.2022 angesetzt wurde [@Voss_sdg].

## Quellen
