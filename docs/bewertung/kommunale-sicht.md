---
sidebar_position: 3
description: Kommunalverwalungsspezifische Einordnung
---

# Potenzial und Problematik aus kommunaler Sicht

Die Kommunen stehen vor der besonderen Aufgabe trotz zunehmender personeller und finanzieller Zwänge eine Vielzahl von Verwaltungsprozessen für die Bürger:innen sicherzustellen. Die dazu notwendige Verwaltungsmodernisierung erfordert eine ausgewogene und vorausschauende Ressourcen- und Digitalisierungsstrategie.

Grundlage für eine erfolgreiche Nutzung der erheblichen Potenziale der Digitalisierung sind Kenntnisse der verschiedenen Digitalisierungskonzeptionen der föderalen Akteur:innen, die dazu geschaffenen Institutionen und die jeweiligen Kompetenzen, die vorgesehenen Komponenten und die beabsichtigte Vernetzungsstruktur. Erkenntnisse daraus müssen einfließen in eine fundierte kommunale Umsetzungsstrategie für ein angepasstes strategisches Vorgehen und zur Vermeidung von Fehlengagements.

Die Gründe für die angestrebte Verwaltungsmodernisierung und die sich daraus ergebenden vielschichtigen Aufgaben der beteiligten Akteur:innen sind bereits ausreichend publiziert. Schwerpunkt soll daher an dieser Stelle ein Ausschnitt der sich ergebenden Potenziale und der damit einhergehenden Herausforderungen für die mit der praktischen Umsetzung befasste Kommunalverwaltung sein.

Der wesentlichste Aspekt ist hierbei die erforderliche Aufarbeitung der bisherigen Prozesse und das Einbringen in das Korsett einer möglichst standardisierten Antragsstrukturierung, Erfassung und Übertragung. Dies beinhaltet die verwaltungsübergreifende Vereinheitlichung von Antragsvoraussetzungen und -bestandteilen bei gleichen Fällen auf der einen Seite und die sich durch die medienbruchfreie Weiterverarbeitung digitaler Daten ergebenden Möglichkeiten zur Prozessoptimierung auf der anderen Seite. Nicht nur die Erleichterungen für den:die Bürger:in, auch die positiven Auswirkungen auf den immer knapper werdenden Ressourceneinsatz der Verwaltungen sind hier besonders herauszustellen.

Die Abschöpfung dieser Potentiale erfordern nicht nur anfänglich wenig messbare Investitionen in eine zunächst noch als unklar empfundene Umsetzungssystematik, sondern auch die Bereitschaft, bestehende Strukturierungen aufzubrechen. Dies ist nur durch eine konsequente Unterstützung der jeweiligen Führungsebenen leistbar. 

Voraussetzung dafür sind aber klare Vorgaben von Bund und Ländern, an denen es gefühlt an vielen Stellen noch mangelt und in deren Folge sich viele Kommunen mit einer für sie nicht überblickbaren Aufgabenfülle allein gelassen fühlen. Es geht dabei in erster Linie nicht nur um die quantitativen Vorgaben zur Umsetzung, sondern vor allem auch um die momentanen Unklarheiten bei den vorgesehenen technischen Komponenten und deren teilweise noch sehr holprigen Zusammenwirkung. Auch stößt man in den einzelnen Umsetzungen auf formelle Hindernisse und Schranken, die, wenn sie nicht normativ beseitigt werden können, erhebliche technische Lösungsaufwände und damit auch Akzeptanzerschwernisse nach sich ziehen. 
 
Zwar werden diverse Hürden insbesondere mit der in „[EfA](https://leitfaden.ozg-umsetzung.de/pages/viewpage.action?pageId=12587267)“-Projekten gebündelten Kraft vieler Akteur:innen und Hierarchien zunehmend überschritten, doch sind derartige Lösungen nicht immer nachnutzbar oder technisch bzw. verwaltungsrechtlich verallgemeinerbar.
Darüber hinaus sind viele, aus einer nutzer:innenorientierten Sicht unabdingbare Rahmenbedingungen, wie das Registermodernisierungsgesetz, nicht verfügbar oder wie bei den Nutzerkonten in verwirrender Vielzahl vorhanden.
Ebenfalls stellt sich in zunehmendem Maße die Frage nach der Bewältigung der mit der Nutzung digitaler Dienste und der notwendigen Ertüchtigungen von mitunter schon sehr antiquierten Fachverfahren einhergehenden Kosten, die durch die aufgelegten Förderprogramme nur bedingt aufgefangen werden können. 

Dennoch gelingen in der Gesamtbetrachtung aller Verwaltungsorganisationen in Deutschland viele interessante Lösungen, deren Erfahrungspotenzial verallgemeinert und zugänglich gemacht werden muss. Viele Verwaltungen teilen die Einschätzung, dass, wenn die Werkzeuge beherrscht und die Kosten geklärt werden, diese Art der Verwaltungsmodernisierung alle Beteiligten begeistern kann!
