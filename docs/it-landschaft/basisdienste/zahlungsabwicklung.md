---
sidebar_position: 3
description: Übersicht und technische Beschreibung von ePayBL
---

# Zahlungsabwicklung und ePayment

## Überblick

Die föderale Struktur der Bundesrepublik führt dazu, dass für den Bund, die Länder und die Kommunen unterschiedliche Rahmenbedingungen und Voraussetzungen für die Einführung von elektronischen Bezahlmöglichkeiten (ePayment) existieren. Das sind z.B. unterschiedliche vergaberechtliche sowie haushaltsrechtliche Regelungen [@BSIePayment].

Zur besseren Übersicht existiert eine Landkarte der ePayment-Lösungen (siehe Grafik), die für jedes Bundesland eine eigene Lösung aufzeigt [@BSIePayment]:

![](/assets/Rahmen/epayment_landkarte.jpg)

Die Regulierung von Zahlungsdiensten und -dienstleistern beginnt auf EU-Ebene mit der Payment Services Directive 2 (PSD2) [@BSIePayment]. In der Bundesrepublik setzt das Zahlungsdiensteaufsichtsgesetz (ZAG) die PSD2-Richtlinie in nationales Recht um [@BSIePayment]. Die Bundesländer verabschieden ihre eigenen E-Government-Gesetze, woraus sich teils abweichende landesrechtliche Regelungen ergeben [@BSIePayment].

Ein grundsätzlicher Zahlungsablauf sieht folgendermaßen aus[@BSIePayment]:

![](/assets/Rahmen/epayment_ablauf.jpg)

dieser kann je nach Zahlungsanbieter leicht abweichen:

Ein umfassender Vergleich vieler Zahlungsanbieter inkl. Kosten und Vor- und Nachteilen mit Handlungsempfehlungen und einer Checkliste ist hier zu finden [@BSIePayment].

## ePayBL

ePayBL [@ePayBLWebsite] implementiert eine Schnittstelle zwischen möglichen Online-Anträgen und Zahlungsdienstleistern. Durch diese Abstraktionsschicht soll vermieden werden, dass sich Abhängigkeiten zwischen bestimmten Leistungen und einzelnen Zahlungsdienstleistern einstellen. Eine Entwickler:innengemeinschaft aus Bund und zehn Bundesländern unter der Leitung des Freistaats Sachsen enwickelt und betreibt die Plattfrom. 

ePayBL bindet derzeit die Zahlungsverfahren PayPal, Kreditkarte, giropay, paydirekt, Vorkasse, Rechnung und SEPA-Lastschrift ein. Im Zusammenhang mit Vor-Ort-Leistungen kann auch ein Kassenbuch zur Barzahlung angebunden werden. Die Anbindung an eine Leistung kann auf zwei Arten erfolgen. Einerseits kann die Bezahlfunktion durch den sogenannten Konnektor direkt in die Benutzeroberfläche der jeweiligen Anwendung integriert werden. Andererseits besteht die Möglichkeit, eine fertige Bezahlseite als Weiterleitungsziel zu verwenden.

ePayBL setzt sich aus den folgenden Modulen zusammen:
- Kern,
- Konnektor,
- Paypage,
- SEPA-Mandatsverwaltung,
- Webshop.

Der Zusammenhang wird aus untenstehender Abbildung deutlich [@ePayBLWebsite]:

![](/assets/Rahmen/ePayBL-Uebersicht.jpg)

- der Konnektor ist über eine RestAPI mit dem ePayBL-Server (enthält Kern, Portal, Paypage) verbunden [@ePayBLIntegrationshandbuch].
- das ePayBL Portal ist unter folgenden Links abrufbar:
    - Portal ePayBL-Referenzsystem: https://refepayment.thueringen.de/portal/
    - Portal ePayBL-Produktivsystem: https://epayment.thueringen.de/portal/
    - den Zugang dafür bekommt man dafür auf Antrag bei dem teilnehmenden Bundenland (Bsp für Thüringen Thüringer Landesrechenzentrum unter [epayment@tlrz.thueringen.de](mailto:epayment@tlrz.thueringen.de))



### Technische Details
Die Datenübermittlung mit ePayBL wird über den [XÖV](/it-landschaft/basisdienste/datenuebermittlung/datenstandards/xoev.md)-Standard XFinanz realisiert. Die XFinanz-Nachrichten können über die Protokolle HTTPS bzw. SCP transportiert werden. Unverschlüsselte Protokolle wie z.B. FTP sind aus Sicherheitsgründen nicht zugelassen.

Die Verantwortung für Betriebsdetails wie z.B. die Rollen- und Rechtevergabe liegt bei den Bundesländern. In Thüringen ist beispielsweise das TLRZ zuständig. Die Aufgabe kann allerdings auch an IT-Dienstleister bzw. Betreiber von Fachverfahren delegiert werden.

Der Kommunikationsfluss aus technischer Sicht ist aus folgender Abbildung ersichtlich [@ePayBLIntegrationshandbuch]:

![](/assets/IT-Landschaft/epaybl_systemarchitektur.png)

## Quellen
