---
sidebar_position: 2
description: Eine Plattform zur Vernetzung von IT-Systemen der förderalen IT-Architektur
---

# FIT-Connect

## Allgemeines

FIT-Connect ist eine Plattform zur Nutzung und Integration von IT-Systemen der förderalen IT-Architektur. Entwickelt und betrieben wird diese von der [FITKO](https://www.fitko.de/). Sie ermöglicht einen sicheren Datentransport von Antragsteller:innen, über Onlinedienste und Unternehmenssoftware, zu Fachverfahren auf Basis von RESTful APIs. Die Plattform kann auch bei Fachverfahren mit eigenem Onlinedienst verwendet werden. Darüber hinaus ist sie [open-source](https://git.fitko.de/fit-connect) und beinhaltet eine ausführliche [Dokumentation](https://docs.fitko.de/fit-connect/docs/). Aus diesem Grund beschränkt sich dieses Kapitel auf einen Überblick und besonders relevante Betrachtungen.

## Leistungsumfang

FIT-Connect beinhaltet einen Routingdienst über eine Routing API. Bei diesem wird das zuständige Fachverfahren (als Empfänger) mithilfe von [PVOG](/docs/it-landschaft/portale/pvog) (Portalverbund Online-Gateway) bzw. [DVDV](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/dvdv) ermittelt, falls im Vorfeld nicht bekannt ist, wer zuständig ist. 
Zusätzlich zum Routingdienst kommt der Zustelldienst, welcher über eine Submission API verfügt. Hierfür existiert eine bidirektionale 1:n-Verbindung, wobei sowohl der Sender als auch der Empfänger sich nur einmalig anbinden müssen und im Anschluss bei allen anderen Teilnehmern Nachrichten empfangen können und ebenfalls an diese Nachrichten senden können. Dabei erhält der Sender Informationen über den Empfänger. Zum Beispiel, welche Datenformate erlaubt sind, ob es einen Rückkanal gibt und wie der öffentliche Schlüssel aussieht. Die API nimmt clientseitig verschlüsselte Antragsdaten vom Sender entgegen, optional wird der Empfänger informiert, wenn neue Daten eingegangen sind. Der Empfänger kann dann die Antragsdaten von der API abfragen und clientseitig entschlüsseln. Das Ereignisprotokoll gewährleistet die Nachverfolgbarkeit durch die Dokumentation aller erfolgten Schritte. Ebenso ist eine Fehleranalyse auf Basis von HTTP-Statuscodes möglich.

## Nutzung

Um FIT-Connect zu nutzen, muss das System erst an eine Testumgebung angebunden werden. Anschließend kann beantragt werden, das eigene System der Produktivumgebung hinzuzufügen. Hierfür muss ein Verwaltungs-PKI-Zertifikat beantragt werden. 
Die Anbindung erfolgt über das Self-Service-Portal. Sender und Empfänger müssen sich einen API-Client anlegen, dieser ist technisch notwendig. Der Empfänger muss zusätzlich einen Zustellpunkt anlegen. Der Zustellpunkt enthält folgende Informationen:

* Kontaktdaten
* Antwortkanäle
* öffentliche Schlüssel für Verschlüsselung und Signatur
* Verwaltungsleistung
* Datenschema
* (optional) Callback-URL zur Benachrichtigung des Empfängers bei Eingang von Anträgen

Anschließend werden die Addressierungsinformationen in die Redaktionssysteme eingefügt.

## Sicherheit

### Sicherheitspolicies

Security by Design ist integraler Bestandteil von FIT-Connect [@FITConnectWebsite]. Alle Infrastrukturkomponenten und SDKs sollen unter offener Softwarelizenz veröffentlicht werden [@FITConnectVorstellung].

### Implementierung
Um die Integration zu vereinfachen, bietet FIT-Connect den Code und die Dokumentation in einem [Gitlab-Projekt](https://git.fitko.de/fit-connect) an. Dazu gibt es eine [Testumgebung](https://portal.auth-testing.fit-connect.fitko.dev/login), die zwar frei genutzt werden kann, aber nicht für produktive Tätigkeiten geeignet ist.

Bei Fragen zur Architekturgestaltung sollten die föderalen IT-Architekturrichtlinien betrachtet werden. Die Architektur "basiert auf bestehenden Architekturrichtlinien des Bundes und der Länder" (siehe [hier](https://www.fitko.de/fileadmin/fitko/foederale-koordination/gremienarbeit/Foederales_IT-Architekturboard/) auch für ausführliche Informationen). Für die Umsetzung des FIT-Connect-Projekts wird die agile Methode Scrum verwendet [@FITConnectSachstandsbericht]. Die Produktivinfrastruktur soll ab Q3/Q4 2022 zur freien Nutzung mit Nutzungsbedingungen und mit validierter Identität freigeschaltet werden.

#### APIs

FIT-Connect bietet zwei APIs für die sichere Übertragung von verschlüsselten Daten, und zwar die Submission-API und die Routing-API. Der Ablauf des Datenübertragungsprozesses zwischen dem Client und Fitconnect wird in https://docs.fitko.de/fit-connect/docs/sending/overview ausführlich erläutert.

FIT-Connect ermöglicht auch Anbindungstests für die beiden APIs:  
- Submission API: https://submission-api-testing.fit-connect.fitko.dev
- Routing API: https://routing-api-testing.fit-connect.fitko.dev
- Schema der APIs: https://schema.fitko.de/fit-connect/

#### SDKs
SDKs sollen in den folgenden Umgebungen verfügbar gemacht werden: [.Net](https://git.fitko.de/fit-connect/sdk-dotnet), [Java](https://git.fitko.de/fit-connect/sdk-java) und [Javascript](https://git.fitko.de/fit-connect/sdk-javascript).

### Authorisierung und Authentifizierung
Für die Authentifizierung an FIT-Connect ist ein Access Token notwendig, "das beim OAuth-Dienst über die hierfür vorgesehene OAuth-Token-URL abgerufen werden kann", dafür muss ein API-Client im Self-Service-Portal konfiguriert sein.

### Zugriff auf die API mittels Access Token
"Beim Zugriff auf die Submission API muss ein Access Token im Authorization-Header mit Bearer-Authentifizierungsschema gemäß RFC 6750 an den Zustelldienst übermittelt werden".

### Updates
Für eine reibungslose Funktion aller Komponenten sind die folgenden Update-Anweisungen zu beachten:
 - Toolingverbesserungen und UI Updates: ca. alle 2 Wochen
 - Abwärtskompatible API Änderungen: ca. 1x pro Quartal
 - Nicht-abwärts-kompatible API Änderungen: ca 1x im Jahr

### IT-Sicherheits-Standards

Die Übermittlung der hochsensiblen Daten von Online-Anträgen muss nicht mittels Verschlüsselung auf Transportebene erfolgen, sondern durch die Verwendung einer Ende-zu-Ende-Verschlüsselung gemäß BSI TR-03107-1. Bislang gibt es keinen IT-Standard oder ein vom [IT-Planungsrat](https://www.it-planungsrat.de/) definiertes Produkt für einen solchen Prozess, "denn hierfür besteht mit https bereits ein Marktstandard".

#### Kryptografische Verfahren
Die Verschlüsselung der Inhaltsdaten von FIT-Connect wird durch eine hybride Verschlüsselung auf Basis von JSON Web Encryption (JWE) realisiert [@FitConnectDokuEncryption].
Die Verschlüsselung erfolgt auf der Basis des JSON Web Encryption-Standards gemäß RFC 7516 unter Beachtung von TR-02102-1 [@FITConnectSachstandsbericht] [@FitConnectDokuCrypto].

#### Auditierung
Relevante Ereignisse (Events) werden im Ereignisprotokoll (Event Log) erfasst. Beim Abruf des Ereignisprotokolls stellt die API eine Tabelle mit JSON-Web-Token (JWT) gemäß RFC 7519 zur Verfügung[@FITConnectGitlab]. Um eine vollständige Prüfung eines Security-Event-Tokens durchzuführen, muss zwingend sowohl die Einhaltung der (kryptografischen) Vorgaben als auch die Signatur geprüft werden. Für die Bereitstellung von signierten Statusinformationen über Security Event Tokens gemäß RFC 8417 wird der Standard JSON Web Signature eingesetzt [@FitConnectDokuCrypto].

Die Überprüfung der Signatur alter Events ist nur möglich, wenn alte Public-Keys nicht gelöscht werden (nur eine Empfehlung!) [@FITConnectWebsite]. Die Prüfung der Signatur des SET (Security-Event-Token) ist abhängig vom ausstellenden System (Zustelldienst, Subscriber oder Sender).
Sowohl die Prüfung der kryptografischen Vorgaben als auch die Prüfung der Signatur darf keinesfalls ausgelassen werden [@FITConnectGitlab].

Das Erzeugen eines SET (Security-Event-Token) besteht aus drei Schritten: Set anlegen, Event Payload hinzufügen und SET erstellen und signieren [@FITConnectGitlab]. Zur Signaturerstellung wird der Signaturalgorithmus RSASSA-PSS mit SHA-512 und MGF1 mit SHA-512 verwendet. Vorgabe gemäß BSI TR-02102 in der Version 2021-01 (Stand 24. März 2021) [@FITConnectGitlab].

### Datenhaltung
Ein regelmäßiges Backup wird nachts durchgeführt, daher ist der Zustelldienst für mehrere Minuten nicht verfügbar [@FITConnectWebsite]. Das genaue Intervall ist nicht bekannt.


### Zertifikatsbeantragung

Zertifikate [@FitConnectDokuZertifikate] aus der V-PKI werden ausschließlich an Behörden ausgestellt. Dazu ist die Identifizierung durch die Behörde mittels Behörden-Siegel ("Behörden-Ident") notwendig. Nach der Antragstellung wird der Antrag an die RA weitergeleitet, die für die Genehmigung der Erstellung des Zertifikats zuständig ist. Der Antrag erfolgt [hier](https://doi.telesec.de/doi/ee/login/displayLogin.html), für Thüringen beim TLRZ.
Das Zertifikat wird mit SHA-512 gehasht mit einer Schlüssellänge von 4096 Bit und einem Sperrpasswort versehen und kann über das DOI-Netz oder über einen öffentlichen Verzeichnisdienst (optional) zugänglich gemacht werden. Nach der Erstellung kann das Zertifikat als PKCS-12-Container heruntergeladen werden.  

#### Anforderungen
Ein Zertifikat kann nicht mehrfach verwendet werden, sondern ein Zertifikat pro Fachverfahren.
Ist ein Fachverfahren jedoch für mehrere Zustellungspunkte zuständig, kann ein Zertifikat für alle diese Zustellungspunkte verwendet werden. Das Vertrauen innerhalb der Behörden muss dazu gegeben sein [@FITConnectWebsite]. Jede Software, die einen entsprechenden Adapter bereitstellt und von einer Behörde mit dem Empfang von Anträgen beauftragt wird, kann Anträge über FIT-Connect empfangen.

#### Vorgaben für eingesetzte X.509-Zertifikate [11]

Das X.509-Zertifikat [@FitConnectDokuZertifikate] wird für den JSON Web Key zur Verschlüsselung der Antragsdaten und auch für die Überprüfung der Signatur der digitalen Empfangsbestätigungen eingesetzt. Bei der Erstellung müssen die Regeln und Standards von BSI TR-02103 beachtet werden.
JSON Web Encryption erfolgt gemäß RFC7516 und für X.509 gilt Hashfunktion SHA-512, Schlüssellänge 4096 Bit, RSASSA-PSS Signaturalgorithmus, Schlüsselverwendung: keyEncipherment für (asymmetrische) Verschlüsselung; digitalSignature sowie nonRepudiation für Signaturen.

### Datenschutz
Die Datenschutzerklärung ist zu finden unter folgendem [Link](https://www.fitko.de/datenschutz).

#### Löschkonzept 
Bei den Löschfristen unterscheidet man zwischen drei Fällen [@FITConnectWebsite] [@FitConnectDokuLoeschung]: 
Im Falle einer erfolgreichen Abholung einer Einreichung durch ein empfangendes System wird dieser zustelldienstseitig nach 7 Tagen gelöscht. Bei unvollständigen Einreichungen, die durch das sendende System (Onlinedienst) nicht abgeschlossen wurden, werden diese nach einem Tag gelöscht. Bei nicht erfolgter Abholung einer Einreichung wird diese nach 21 Tagen gelöscht.

### Responsible Disclosure
FIT-Connect bietet die Möglichkeit, Schwachstellen schnellstmöglich zu melden und auch zu bearbeiten. Die Meldung wird wie folgt behandelt [@FitConnectDokuResponsDis]:  
- Innerhalb von drei Werktagen nach der Meldung bekommt man eine Empfangsbestätigung.
- innerhalb von drei Werktagen nach der Empfangsbestätigung erhält man eine Reaktion, die eine Beurteilung der Meldung und das voraussichtliche Datum der Lösung beinhaltet.      

Es bestehen folgende Kontaktmöglichkeiten:
- Per E-Mail (it-sicherheit@fitko.de) oder Telefon (+49 (69) 401270 139) melden
- Security.txt mit PGP-Schlüssel verfügbar unter https://docs.fitko.de/.well-known/security.txt
- Der IT-Sicherheitsbeauftragte ist Marco Holz.

### Beziehung zu anderen Basisdiensten
- Governikus MultiMessenger [@FITConnectWebsite]
- Deutsches Verwaltungsdiensteverzeichnis (DVDV): über eigenen Microservice mit separaten Datenbestand angebunden [@FITConnectWebsite]
- Portalverbund Onlinegateway (PVOG) [@FITConnectVorstellung]
- Die Submission API übermittelt die Daten an das DVDV und den Dienst des jeweligen Fachverfahrens [@FITConnectWebsite]
- Die Submission API erhält Daten aus dem EfA Onlinedienst und dem FitConnect SelfServiceProtal [@FITConnectWebsite]
- Über die Routing API erfolgt ein bidirektionaler Datenaustausch zu DVDV, dem Portalverbund Onlinegateway und dem EFA Onlinedienst [@FITConnectWebsite]

## Quellen
