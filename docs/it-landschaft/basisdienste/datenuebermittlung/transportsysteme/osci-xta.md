---
sidebar_position: 1
description: Zwei sich ergänzende Transportstandards
---

# OSCI-XTA


## OSCI

OSCI entstand im Rahmen des *Media@Komm*-Städtewettbewerbs. Es ist die Spezifikation eines deutschen Transportstandards und wird von der [KOSIT](https://www.xoev.de/) betrieben. Die eigentliche Umsetzung erfolgt in unterschiedlichen Systemen. Das Unternehmen [Governikus](https://www.governikus.de) bietet beispielsweise viele Produkte in diesem Bereich an und ist zentraler Player. 
Eine zentrale Herausforderung ist die sichere Kommunikation in unsicheren Netzen. Hierfür gibt es verschiedene Grundprinzipien von OCSI. Diese basieren auf einer PK-Infrastruktur der Verwaltung. Wichtige Komponenten sind:

* Sicherheit
* Authentisierung
* Transparenz und Nachverfolgbarkeit
* Protokollierung

OSCI ist verbreitet bei verschieden Standards der Innenverwaltung, z.B. XMeld oder XPersonenstand. Diese orientieren sich eng am OSCI-Konzept, haben aber ein eigenenes Verständnis, wie dieses anzuwenden ist und nutzen dabei [DVDV](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/dvdv) als zentrales Instrument der technischen Adressierung. OSCI ist ein kleiner Metadatensatz, welcher wie ein Umschlag um die Fachdaten gelegt wird. Er wird nicht integriert, sondern als Schnittstelle genutzt.[@OSCI]

## XTA
XTA soll den Austausch von Daten standardisieren. Dabei bezieht sich der Standard im speziellen auf Transport- und Fachverfahren. Er basiert auf dem [4-Corner-Modell](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiQgJeYm8CAAxUF2wIHHTEuCAAQFnoECBkQAQ&url=https%3A%2F%2Fwww.oeffentliche-it.de%2Fdocuments%2F10181%2F188095%2FTechnische%2BPerspektiven%2Bder%2BRegistermodernisierung.pdf%2Fe49d4846-826e-db70-fc46-cf3abafd66ad%3Ft%3D1614960958853%26download%3Dtrue&usg=AOvVaw3JGP8XPWox6VYWDVangKyG&opi=89978449). Die grundlegenden Funktionen sind unter anderem:

* Prüfen, ob ein Zugang bei dem:der Sender:in eingerichtet und verfügbar ist.
* Die Adresse eines:r Empfänger:in für einen Leser ermitteln und Auskunft erhalten, ob dieser Leser einen bestimmten Nachrichtentyp grundsätzlich annehmen kann.
* Eine eindeutige Kennung für einen Transportauftrag erzeugen.
* Das synchrone/asynchrone Senden von Nachrichten.

Weitere Details sind unter den [Spezifikationen](https://www.xoev.de/downloads-2316#XTA) nachzulesen. 
Über Webserviceprofile, in welchen bestimmte Kriterien ausgewählt werden können, kann eine formale Spezifikation der Anforderungen an den Transport erfolgen.
Durch diese Spezifikation kann ein einfacher Wechsel zu AS4 erfolgen. Kriterien sind dabei etwa:

* Sicherheitsniveau,
* Protokollierungsniveau.

Durch diese Standardisierung wird eine Vereinheitlichung der verschiedensten Schnittstellen in der öffentlichen Verwaltung erreicht.  [@XTA]

## Zusammenspiel OSCI-XTA

OSCI behandelt insbesondere die Nachrichten selbst. Es macht sie sicher, rechtsbindend und vertraulich in der Kommunikation innerhalb unsicherer Netzwerke. XTA beschäftigt sich dagegen mit dem Transport der Nachrichten. Es unterstützt die Automatisierung von Fachverfahren und Weiterverarbeitung der Nachrichten. Beide Standards nutzen HTTP bzw. HTTPS als Übertragungsweg. Betrieben werden beide Standards von der KOSiT. 

## Quellen
