---
sidebar_position: 3
description: Die Datenstandards der FIM-Informationen
---

# FIM-Standards

## XZufi
 
Als XZufi wird der Datenübertragungsstandard der XÖV für das Finden von Zuständigkeiten und Beschreiben von Leistungen bezeichnet. Im FIM-Kontext wird er zur Übermittlung von Informationen über [FIM](/docs/grundlagen-und-rahmen/fim)-Leistungen genutzt. Sowohl die XML Schema-Definitionen (XSD) als auch die Spezifikationen des Standard sind im XRepository unter folgender Webadresse abrufbar: 

* https://www.xrepository.de/details/urn:xoev-de:fim:standard:xzufi

Wie die meisten XÖV-Standards beschreibt auch XZufi verschiedene Nachrichtenverläufe. Im [OZG](/docs/grundlagen-und-rahmen/ozg)-Kontext sind die Nachrichtenverläufe für

* die Übermittlung von Leistungsdaten und
* die Übermittlung von Organisationseinheitsdaten inklusive Zuständigkeitssucher

wichtig. 
Bei dem Usecase "Übermittlung von Leistungsdaten" werden Daten der Leistungen angefragt. Über die Nachricht: "leistungen.antwort.leistung.040104" werden anschließend Daten über die Leistungen zurückgegeben. Diese enthalten verschiedenste Daten wie: ID, ProzessID oder den Modultext, welcher die Beschreibung der Leistung enthält, weiterführende Links und anderes.  
Bei dem Usecase "Übermittlung von Organisationseinheitsdaten inklusive Zuständigkeitssucher" wird die zuständige Stelle abgefragt. Neben der Anfrage: "zustaendigkeiten.anfrage.organisationseinheit.040402", welche die Organisationseinheit abfragt, gibt es die Nachricht "zustaendigkeiten.antwort.organisationseinheit.040406", welche Rückmeldung über die zuständige Organisationseinheit gibt. Hier wird eine Auswahl an zuständigen Organisationseinheiten zurückgegeben, welche ID, Name, Anschrift und andere Informationen besitzen. Verschiedene Zahlungsmöglichkeiten, Arten der Bankverbindung und anderes ist ebenfalls angedacht. [@XZufi]


## XProzess

XProzess ist eine der drei FIM-Informationen. Wie es der Name bereits verrät, handelt es sich um das Artefakt des Bausteins Prozesse. Es basiert auf dem XProzess XÖV-Standard. Sowohl die XML Schema-Definitionen (XSD) als auch die Spezifikationen des Standards sind im XRepository unter folgender Webadresse abrufbar: 

* https://www.xrepository.de/details/urn:xoev-de:mv:em:standard:xprozess

Diese beinhalten den Aufbau der XProzess-Nachricht. Das XProzess-Artefakt spielt in FIM eine zentrale Rolle. Es enthält sowohl Verweise auf Datenfelder als auch Informationen über den LeiKaschlüssel der FIM-Leistungen. Beim Auslesen der FIM-Artefakte spielt es eine zentrale Rolle.
Im Groben besteht diese aus drei Kategorien: Nachrichtenkopf, Prozesskatalog und Prozessbibliothek. Im Nachrichtenkopf sind allgemeine Elemente vorhanden, wie Autor:in der Nachricht und UUID zur Identifizierung. Im Prozesskatalog gibt es u.a. die "nachrichtenUUID". Der Inhalt entspricht dem LeiKaschlüssel der beschriebenen Leistung und kann genutzt werden, um das Artefakt mit seinen anderen FIM-Artefakten zu verknüpfen. Weiter findet man hier die Version und die verschiedenen Handlungsgrundlagen, welche zur Erstellung des Prozesses geführt haben.
In der Prozessbibliothek findet man unter dem Knoten Prozess weitere wichtige Informationen, darunter über den Prozesssteckbrief. Dieser enthält nicht nur die "prozessstrukturbeschreibung", wodurch die einzelnen Prozessschritte ihre Referenzaktivitätengruppen zugewiesen bekommen. Unter Auslöser, findet man den Knoten "FormularID", welcher eine Referenz zu den Datenfeldern beinhalten soll. Ebenfalls in der "prozessstrukturbeschreibung" ist das "Prozessmodell" enthalten. Dieses enhält Base64 codierte Zip-Files (erst gezipt, dann das Zip-file Base64 codiert). Hier sind mehrere Dateien vorgesehen: eine PDF als Ansicht des Prozesses, eine PDF für die Beschreibung der Prozessschritte und eine BPMN2.0-Datei im XML Format. In der BPMN2.0-Datei ist im standartisierten BPMN2.0-Format der Prozess formalisiert und maschinenlesbar abgelegt, allerdings ist dieser nicht ausführbar. 
Ebenfalls sind im XProzess-Artefakt Subprozesse beschrieben und weiter mit FIM-Informationen, wie Rechtsgrundlage, ID und Referenzaktivitätengruppe (RAG) annotiert. Um den Subprozess zu identifizieren, gibt es im FIM-Artefakt den Knoten "subProzessID" und im BPMN2.0-File den Knoten "subProzess", welche mit einer UUID versehen sind. Somit können die Subprozesse aus dem FIM-Artefakt mit den Subprozessen des BPMN2.0-Files gematched werden. [@XProzess]


## XDatenfeld

XDatenfelder werden zur Übertragung von den FIM-Artefakt-Datenfeldern genutzt. Er basiert auf dem XÖV-Standard XDatenfeld. Sowohl die XML Schema-Definitionen (XSD) als auch die Spezifikationen des Standards sind im XRepository unter folgender Webadresse abrufbar: 

* https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder

Der Aufbau des XDatenfeldes ist rekursiv definiert. Unter dem Grundkonstrukt "stammdatenschema" gibt es verschiedene weitere Knoten für Informationen, wie Name und ID. Eine weitere Besonderheit besteht darin, dass auf dieser Ebene Regeln definiert werden können, einerseits als Prosatext, andererseits als JavaScript (was bisher nicht aufgetaucht ist). Diese Regeln beziehen sich auf die Datenfeldstrukturen, die nachfolgend definiert werden.
Unter dem Knoten Struktur wird die Datenfeldgruppe mit eigenen Regeln, Informationen, Freigaben und weiteren Unterstukturen definiert. Datenfelder enthalten Multiplizitäten. "1:1" bedeutet zum Beispiel, dass diese Gruppe ausgefüllt werden muss, während "0:1" ein optionales Feld kennzeichnet. Wie bereits erwähnt, findet hier die Rekursion statt. Datenfeldgruppen enthalten wieder Datenfeldgruppen. Diese Rekursion endet bei den Datenfeldern, welche dann Felder, wie Textboxen oder Auswahllisten usw. definieren. Auch sie besitzen Multiplizitäten.
Mit dieser Definition baut man sich eine Baumstruktur auf, welche das momentane Datenfeld als Root-Element besitzt und weitere vorher definierte Datenfelder als Unterelemente besitzt. Diese haben ihre eigene fachliche Freigabe. [@XDatenfeld]

# Quellen
