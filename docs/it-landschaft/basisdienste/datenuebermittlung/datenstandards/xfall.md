---
sidebar_position: 2
description: Ein universelles Datenformat zur Übermittlung von Daten und Dokumenten
---

# XFall

XFall stellt ein universelles Datenformat zur Übermittlung von Daten und Dokumenten bereit. Das Format ist in zwei Teile unterteilt: XFall-Daten für Daten, und XFall-Container für Dokumente. Sowohl die XML Schema-Definitionen (XSD) als auch die Spezifikationen des Standards sind im XRepository unter folgenden Webadressen abrufbar: 

* https://www.xrepository.de/details/urn:xoev-de:it-plr:standard:xfall-daten
* https://www.xrepository.de/details/urn:xoev-de:it-plr:standard:xfall-container

Bei XFall-Daten wird eine Formulargruppe pro Prozess abgebildet. Es wird sowohl ein standardisierter Nachrichtenverlauf als auch eine standardisierte, strukturierte Nachrichtenstruktur für ein bestimmtes Fachverfahren beschrieben. Eine Nachricht steht für ein Formular mit Daten zum Transport zwischen Schnittstellen einzelner Fachverfahren. Für verschiedene Verfahren, u.a. für Vollmachten von KFZ-Verfahren wurden schon Nachrichten definiert. Um einen gewissen Standard zu setzen, wurde ein Baukasten geschaffen, auf dessen Grundlage neue standardisierte Nachrichten erstellt werden können. Dieser beinhaltet viele verschiedene Datenfelder [@xFallDaten], wie: 

* Anschrift
* Ausweisdokument
* Bankverbindung 


XFall-Container ist ein Datenformat, welches allgemeine Datenformate annimmt, um diese anschließend zu verschicken. Dabei steht die "Implementation" im Vordergrund. Dies ist eine anonyme Klasse, welche eine nicht in XFall definierte Datenstruktur enthält. Somit ist es Dritten möglich, ihre eigene Struktur für ihr Fachverfahren zu verwenden, dies aber in einer standardisierten Umgebung zu verschicken. 
Wie der Name schon sagt, wirkt XFall-Container hier als Containerstruktur zum Versenden von Nachrichten. Dabei werden verschiedene Strukturen für verschiedene Szenarien bereitgestellt, u.a.:

* Änderungen des Antrags transportieren
* freie Nachricht transportieren
* bestimmten Teilantrag anfragen

Dabei ist das Feld "Header" immer vorhanden. Diese Feld enthält eindeutige Informationen über die zu übertragende Nachricht, nämlich die "messageID". Die "messageID" fungiert als eindeutige Kennung der Nachricht und gilt als Schutzmechanismus gegen Doppeleinreichung von Anträgen. Zudem erhöht sie die Nachvollziehbarkeit in Protokollsystemen. Sie darf nur einmal vergeben werden. [@xFallContainer]

# Quellen 
