---
sidebar_position: 5
description: Eine Schnittstelle für die Interoperabilität von Authentifizierungsdiensten
---

# eIDAS Nodes
 
Über die eIDAS Nodes wird die Interoperabilität gewährleistet. Gemäß der Notifizierung sind alle EU-Mitgliedstaaten seit dem 29.09.2018 verpflichtet, ihre eigenen Verwaltungsleistungen für die deutsche Online-Ausweisfunktion zu öffnen [@BSIeIDAS]. Zusätzlich sorgen die eIDAS Nodes für die mitgliedsstaatenübergreifende Kommunikation. Die Umsetzung der Nodes für das Mitgliedsland Deutschland ist bereits im Status "in Produktion" [@eIDASEnablesEU]. Die eIDAS Middleware dafür ist quelloffen von der Firma Governikus entwickelt.
Im Folgenden finden sich die wichtigsten Ressourcen zu eIDAS Nodes:
 - Die eIDAS Middleware ist quelloffen [auf Github verfügbar](https://github.com/Governikus/eidas-middleware).
 - Der Handlungs- und Informationsbedarf für deutsche Behörden gemäß der eIDAS Verordnung findet sich im [Personalausweisportal](https://www.personalausweisportal.de/Webs/PA/DE/verwaltung/eIDAS-verordnung-der-EU/handlungs-und-umsetzungsbedarf/handlungs-und-umsetzungsbedarf-node.html).
 - Daraus ergeben sich [häufige Fragen zur eIDAS Anerkennungsverpflichtung](https://www.personalausweisportal.de/SharedDocs/faqs/Webs/PA/DE/eIDAS-Verordnung-Haeufige-Fragen/eIDAS-verordnung-haeufige-fragen-liste.html).
 - Die [technischen Spezifikationen (derzeit v1.2)](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS+eID+Profile) des eIDAS eID Profils, unter anderem das Nachrichtenformat, Architektur, sowie kryptographische Voraussetzungen.
 - Das [eIDAS Nodes Integrationspaket](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS-Node+Integration+Package) in der aktuellen Version 2.5.

## Kommunikation
Die Kommunikation zwischen dem sendenden und dem empfangenden Mitgliedstaat ist wie folgt aufgebaut [@BSIeIDASInterop]:
 - Der sendende Mitgliedsstaat stellt den anderen Mitgliedsstaaten eine Middleware zur Verfügung, in der das nationale eID Schema integriert wird und welche vom empfangenden Mitgliedstaat betrieben wird (Middleware-basierte Integration).
 - Alternativ betreibt der sendende Mitgliedsstaat einen zentralen Proxy, welcher zwischen dem eIDAS-Konnektor des empfangenden Mitgliedsstaats und dem nationalen eID-System (den Service-Providern) des sendenden Staats übersetzt (Proxy-basierte Integration).
![](/assets/IT-Landschaft/eIDAS_Interoperabilitaet_Integrationsszenarien.jpg)

Von den einzelnen Mitgliedstaaten wurden [technische Spezifikationen](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS+eID+Profile) für deren Konnektivität entwickelt  [@eIDASEnablesEU].
Die Spezifikationen für Deutschland wurden durch das [BSI](https://www.bsi.bund.de/DE/Home/home_node.html) entwickelt und in der technischen Richtlinie ["TR-03130 eID-Server"](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03130/TR-03130_TR-eID-Server_Part1.pdf?__blob=publicationFile&v=3) veröffentlicht.

## Quellen
