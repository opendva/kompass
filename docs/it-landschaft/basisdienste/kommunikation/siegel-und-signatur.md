---
sidebar_position: 4
description: elektronische Umsetzung von Vertrauensdiensten
---

# Siegel- und Signaturdienst

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Zum Siegel- und Signaturdienst der Bundesdruckerei sind wenige Informationen verfügbar, weshalb dieses Kapitel erst später noch vervollständigt werden kann. Konkret fehlen z.B. noch Informationen zur Bundesdruckerei PKI [@BDRaufbauPKI] oder der Signatur-Prozess für PDF-Daten für Nachweise und Bescheide in der Behörde.

Ein Tochterunternehmen aus der Bundesdruckerei-Gruppe ist D-Trust. Sie übersetzt die Regulierung in konkrete Produkte [@BDReIDASInfo]. Vertrauensdienste wie digitale Zertifikate, elektronische Signaturen und Siegel werden von ihr auf Basis der eIDAS-Verordnung entwickelt [@BDReIDASInfo]. Diese Vertrauensdienste sind zertifiziert nach ISO 27001 und TÜV TSI-Level-3.
Für die Verwaltung hat die D-Trust GmbH verschiedene Produkte im Portfolio [@dtrustBuergeramtInfo]:
- AusweisIDent Online: online mit dem Personalausweis identifizieren
- AusweisIDent Vor-Ort: Ausweisdaten vor Ort auslesen
- sign-me: Elektronische Signatur für digitale Dokumente
- Signaturkarten: unterzeichnen von digitalen Dokumenten mit Offline-Karten

Die Dienste AusweisIDent Online sowie sign-me stehen im Kontext der digitalen Verwaltung und werden im Folgenden genauer beschrieben.

## AusweisIDent Online
Der Dienst "AusweisIDent Online" wird entwickelt von Governikus zusammen mit D-Trust [@dtrustAusweisIDentOnlineInfo]. Der AusweisIDent Service wird über die AusweisApp2 (entwickelt im Auftrag des BMI) gestartet und die Daten aus dem Personalausweischip werden mit einem Lesegerät oder einem NFC-fähigen Smartphone ausgelesen. Die Authentifizierung wird über die Webschnittstelle OpenID Connect (basierend auf OAuth2) bereitgestellt, diese ist integrierbar als API oder SDK. Dieser Dienst ist eine Alternative zu den Nutzerkonten der Länder und des Bundes. Sie können parallel oder einzeln als eID-Services eingesetzt werden.

## sign-me
Sign-me ist eine Signaturplattform bei der Dokumente rechtssicher online unterschrieben werden können. Die Plattform steuert den Signaturprozess und beinhaltet alle Softwarekomponenten. Es sind verschiedene Pakete (basic, advanced, qualified) verfügbar, die an unterschiedliche Vertrauensstufen gekoppelt sind, siehe auch: https://www.d-trust.net/files/dokumente/pdf/produktblatt_sign-me.pdf. Der Signaturprozess ist in folgender Infografik zusammengefasst [@dtrustSignMeInfo]: 

![](/assets/IT-Landschaft/bundesdruckerei_remote_signature_with_sign-me.jpeg)

# Quellen
