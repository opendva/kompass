---
sidebar_position: 2
description: Stand der Chatfunktionen
---

# Chat

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Die Chatkomponente soll Verwaltungskund:innen beim Feststellen der ihnen zustehenden Leistung unterstützen. Angedacht ist sowohl der Livechat mit einem:r Servicemitarbeiter:in der öffentlichen Verwaltung als auch der Einsatz automatisierter Chatbots. Letztere könnten auf AI-Basis entwickelt werden. Bei der Umsetzung soll sich am Chatbot der 115 ortientiert werden. Stand September 2021 wird der Chatbot noch entwickelt. [@FoederaleITLandschaft, p. 35]

# Quellen
