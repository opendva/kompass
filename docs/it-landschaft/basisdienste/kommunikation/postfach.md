---
sidebar_position: 1
descrition: Dienst v.a. für den Empfang von antragsbezogenen Nachrichten
---

# Nutzerpostfach

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Das Nutzerpostfach dient dem:der Bürger:in zum Empfangen von elektronischen Nachrichten, insbesondere für die antragsbezogene Kommunikation und Zustellung von Bescheiden. Hierbei wird der:die Nutzer:in über den Nachrichteneingang per Email informiert. Die Nachrichten werden im XML-Format übertragen und stehen dabei in direkter Verbindung zum sendenden Fachverfahren. Das im Fachverfahren geforderte Vertrauensniveau wird auch zum Einsehen der Nachricht vorausgesetzt.
