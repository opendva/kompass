---
sidebar_position: 6
description: Beschreibung verschiedener Nachnutzungsstrukturen
---

# Integration, Entwicklung und Mitnutzung

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

## Nachnutzung

Für die Nachnutzung sind die Bundesländer gemeinsam mit den jeweiligen Bundesressorts federführend bei der Umsetzung der einzelnen [OZG](/docs/grundlagen-und-rahmen/ozg)-Themenfelder. Zur Nachnutzung gibt es drei Modelle:

| **Name** | **Beschreibung** |
|-------------|-------------|
| **FIM-basierte Eigenentwicklung** | Die Gestaltung und Dokumentation des Onlinedienstes erfolgt nach den FIM-Standards. Die nachnutzenden Länder müssen diese Dienste anhand von *FIM-Artefakten* selbst entwickeln. |
| **Nachnutzbare Software dezentral betrieben** | Die federführende Instanz entwickelt die Software, welche in nachnutzenden Ländern lokal (dezentral) betrieben wird.  |
| **Einer für Alle**| Die entwickelte Software wird zentral im federführenden Bundesland betrieben und ist mandantenfähig. Die nachutzenden Länder werden an diese "angeschlossen". Das Ergebnis der Online-Verfahren wird an die jeweiligen Landes- und Kommunalbehörden weitergeleitet. Bereits nach EfA betrieben werden: VEMAGS, ELSTER, BAföG und ALG II. |

## Organisationstrukturen

Für die Nachutzung gibt es verschiedene Organisationstrukturen. Zum einen gibt es den:die **Themenfeldverantwortliche:n** im federführenden Bundesland. Diese:r ist verantwortlich für die Umsetzung aller OZG-Leistungen seines Themenfeldes. Er:Sie muss frühzeitig sicherstellen, dass selbst entwickelte Leistungen für andere nachnutzbar sind. Er:Sie soll für Transparenz über nachnutzbare Software für andere Länder sorgen. Ebenso kann mit anderen Ländern eine Umsetzungsallianz eingegangen werden. 
Zum anderen gibt es in jedem Land eine:n **OZG-Koordinator:in**. In dessen Zuständigkeit fällt die Zugänglichkeit aller OZG-Leistungen des Landes. Er:Sie entscheidet, welche Dienste selbst entwicket und welche nachgenutzt werden können. Er:Sie ist Ansprechpartner:in für das zentrale OZG-Management des Bundes. 
Zuletzt gibt es eine:n **Leistungsverantwortliche:n** für jede Onlineleistung. Die Entwicklung und Digitalisierung des Dienstes, sowie die operative Umsetzung fällt in seine:ihre Verantwortlichkeit. Er:Sie leitet ein Entwicklungsteam und kann andere Länder in die Entwicklung mit einbeziehen, um sicherzustellen, dass die Nachnutzung gewährleistet werden kann. 

## Technische Strukturen

Die technischen Strukturen sind ein Zusammenspiel aus zentralen Komponenten, also [EfA](https://leitfaden.ozg-umsetzung.de/pages/viewpage.action?pageId=12587267)-Diensten, dezentralen Komponenten, Fachverfahren und Terminvereinbarungsdiensten sowie die Kommunikation über ein standardisiertes Datenformat. Den Bürger:innen werden EfA-Leistungen über das Bundesportal, ein Landesportal oder das einer Kommune angeboten. Die Mandantenfähigkeit der EfA-Leistungen stellt sicher, dass die Datenverarbeitung für alle nachnutzenden Behördern getrennt erfolgt. 
Ebenso werden querschnittlich genutzte Basisdienste, die in diversen Verfahren gebraucht werden, zentral betrieben, anstelle diese mehrfach zu implementieren. Beispiele hierfür sind das Nutzerkonto zur Authentifizierung oder die Bezahlkomponente. Ebenso sollen relevante Register über standardisierte Schnittstellen an zentrale Dienste angeschlossen werden. Der Fit-Store ist eine dieser zentralen Komponenten. Dieser bildet einen Marktplatz für die Kontaktsuche, z.B. für Umsetzungsallianzen. Die Länder können hier ebenso ihre selbstentwickelte Umsetzung anbieten. Somit können nachnutzende Länder passende Anwendungen finden und mit den Entwickler:innenn in Kontakt tretten. 

## EfA-Mindestanforderungen

Um als EfA-Dienst zu gelten, gibt es die sog. *EfA-Mindestanforderungen*. Zum einen betrifft dies das Oberflächendesign. Dies soll länderneutral gestaltet sein. Anhand geeigneter Merkmale soll der Name der zuständigen Behörde und das Wappen der zuständigen Gebietskörperschaft angezeigt werden. 
Eine weitere Anforderung ist die Fachlogik. Der Dienst muss sowohl die Anforderungen der Bundesgesetzte, als auch die landesrechtlichen Zusatzanforderungen der Nachnutzenden erfüllen. 
Die Dritte Anforderung ist die Anbindung eines interoperablen Nutzerkontos für alle Efa-Dienste. Zunächst soll die Anbindung an die Nutzerverwaltung des Bundes erfolgen. Später, wenn diese überall verfügbar ist, ist die Anbindung an die Nutzerverwaltungen der einzelnen Länder geplant. 
Der Datenaustausch soll ebenfalls standardisiert werden und über standardisierte Schnittstellen zwischen Onlinediensten und Fachverfahren geschehen.  
Die letzte Anforderung ist das Routing der Daten. Hierfür sollen Oninedienste, darunter [DVDV](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/dvdv), benutzt werden, welche von den [FIM](/docs/grundlagen-und-rahmen/fim)-Redaktionen gepflegt werden und zur Adressierung von Fachbehörden dienen. Für die sichere Übermittlung der Antragsdaten kann dann das OSCI/XTA Protokoll genutzt werden. Für die Übermittlung und das Routing selbst hat die FITKO [FIT-Connect](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/fitconnect), also einen API-basierten Datenaustausch zwischen Diensten und Behörden geschaffen.

## Rechtlicher Rahmen für Nachnutzung

Die Verwaltungsvereinbarung, die klassische Lösung für länderübergreifende Kooperationen, muss sowohl vom umsetzenden als auch von allen nachnutzenden Ländern unterzeichnet werden. Sie regelt die Zusammenarbeit bei Entwicklung und Betrieb des Dienstes sowie dessen Pflege und Weiterentwicklung. Blaupausen für den OZG-Dienst stehen hierbei zur verfügung. Dies gilt nur für Länder, nicht aber für Kommunen. 
Der Fit-Store, als Neuentwicklung der FITKO, bietet standardisierte Verträge, welche direkt zwischen [FITKO](https://www.fitko.de/) und Ländern abgeschlossen werden können. Hierfür können bereitstellende Länder Dienste in den [FIT-Store](https://www.fitko.de/produktmanagement/fit-store) einstellen. Nachnutzende Länder können diese aus dem FIT-Store beziehen. Die Bereitstellung erfolgt als SaaS. Auch die Anpassung und die Weiterentwicklung ist in den Verträgen geregelt. Die FITKO, welche durch Bund und Länder getragen wird, bietet einheitliche und transparente Verträge für alle. Kommunen müssen auch hier einen Umweg über die Länder gehen. Informationen über die Konditionen und die technischen Voraussetzungen zur Nutzung von Diensten für Nachnutzende sind einsehbar.

## Finanzielle Dimension

Die Erstentwicklungskosten werden vom bereitstellenden Land getragen und vom Bund durch entsprechende Konjunkturpakete gefördert. Die Betriebskosten werden anschließend zwischen den teilnehmenden Ländern aufgeteilt. Die Organisation der Finanzierung liegt im Aufgabenbereich des umsetzenden Landes.
