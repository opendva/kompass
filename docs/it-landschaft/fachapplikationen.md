---
sidebar_position: 3
description: Hier finden Sie eine kurze Definition zum Thema Fachapplikationen sowie die Beschreibung gängiger Anwendungen aus dem Bereich öffentliche Verwaltung.
---

# Schicht: Fachapplikationen

:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Eine Fachapplikation ist im Allgemeinen eine Software, die genutzt wird, um geschäftliche Funktionalitäten auszuführen oder zu unterstützen. Dies können in der föderalen IT-Landschaft konkret Online-Antragsdienste für die Erstellung und den Versand von Anträgen an die öffentliche Verwaltung sein. Fachhapplikationen können jedoch auch Fachverfahren sein, die Anträge entgegennehmen bzw. andere fachliche Prozesse innerhalb der öffentlichen Verwaltung IT-seitig unterstützen.

Nicht zu den Fachapplikationen gehören unterstützende Basisdienste, die fachübergreifende Leistungen bereitstellen. Dies können Dienste für [Nutzerkonten](/docs/it-landschaft/basisdienste/identitaet/nutzerkonto-bund), [Zahlungsverkehr](/docs/it-landschaft/basisdienste/zahlungsabwicklung) (Payment), Willensbekundung oder Verzeichnisdienste sein.

Der Begriff der Fachapplikation ist als eine logische Einheit zu verstehen, die keine Aussage über die technische Komplexität des Informationssystem trifft. 

Synonyme
- Anwendung
- Informationssystem

