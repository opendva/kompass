---
sidebar_position: 4
description: Einführung und Einordnung des Konzepts
---

# Offene Daten in der Verwaltung: Transparenz, Teilhabe und Potenziale

Offene Daten - "Open Data" - sind für jedermann frei
zugänglich und können auf Grund von offenen und diskriminierungsfreien Lizenzen
frei weiterverwendet werden [@BMI_OpenData].
Die Idee offener Daten hat ihre Wurzeln in der Überzeugung, dass Informationen,
die von öffentlichen Stellen unter Einsatz von Steuergeldern gesammelt und verwaltet
werden, der Gesellschaft zur Verfügung stehen sollten. Auf dem Weg zu
"Open Government" und damit verbundener Transparenz, Teilhabe und
Demokratieförderung stellt die Veröffentlichung von Daten als Open Data einen
Meilenstein dar [@Dortmund_OpenData].

Die Bereitsteller dieser Daten sind im Datennutzungsgesetz breit
definiert: Staatliche Stellen, Wirtschaft im öffentlichen Auftrag sowie Wissenschaft
und Forschung fallen ganz oder teilweise unter das Gesetz [@DNG].


## Über 250 Jahre Geschichte

Für Schweden und Finnland verabschiedete das Parlament 1766 das erste Informationsfreiheitsgesetz als Teil der Pressefreiheit.
Haupterrungenschaft war neben der Abschaffung der politischen Zensur der Zugang der Öffentlichkeit zu Regierungsdokumenten [@First_FOIA].

Der Grundsatz der Öffentlichkeit ist in den nordischen Ländern bis heute von zentraler Bedeutung: „In Schweden haben die Bürger durch das Öffentlichkeitsprinzip das Recht auf Einsicht in und Zugang zu Informationen, die sich im Besitz von Behörden befinden. Auf diese Weise können einzelne Personen und Journalisten die Staatsgewalt und die vom Volk gewählten Politiker kontrollieren. Bei uns hat dieses Prinzip, das von zentraler Bedeutung für unsere Rechtsordnung ist, dazu beigetragen, dass es in Schweden nur wenig Korruption und ein hohes Maß an Vertrauen für demokratische Institutionen gibt. Es steht außer Zweifel, dass die offene Gesellschaft die Grundlage für Wirtschaftswachstum und Wohlstand in unserem Land bildet.“, schreibt der stellvertretende Botschafter Schwedens in einem Gastbeitrag zum 250. Jahrestag des Gesetzes [@Netzpolitik_250Jahre_Pressefreiheit].

## Potenziale und Ziele

Im Zuge der Digitalisierung entwickeln sich sowohl die Potentiale von Open Data als auch der Wunsch nach Open Data stetig weiter: „Daten
bilden eine Grundlage eines modernen Staates und einer modernen Gesellschaft“ [@Digitalstrategie].

Durch den freien Zugang zu Informationen können Bürgerinnen und Bürger nicht nur besser informierte Entscheidungen treffen, sondern auch innovative Anwendungen und Dienstleistungen entwickeln. Die Ziele reichen von der Förderung der Transparenz und dem Ausbau demokratischer Teilhabe bis hin zur Stimulierung wirtschaftlicher Entwicklungen durch die Schaffung neuer Geschäftsfelder:  „Die Nutzung von Daten kann neben enormen Mehrwerten für Bürgerinnen und Bürger sowie Wirtschaft und Wissenschaft auch für Staat und Verwaltung erhebliche Mehrwerte generieren. Mit der rasant voranschreitenden Digitalisierung sind die Verfügbarkeit von Daten und ihre Verknüpfung zu bedeutenden Faktoren für unsere moderne Gesellschaft, Wirtschaft und Wissenschaft, aber auch für die tägliche Arbeit von Politik und öffentlicher Verwaltung geworden“ [@Digitalstrategie].

Innerhalb der Verwaltung ermöglichen offene Daten eine effizientere Arbeitsweise, indem sie den Austausch von Informationen zwischen verschiedenen Abteilungen erleichtern. Zudem fördern sie Innovation und verbessern die Qualität der erbrachten Dienstleistungen. Die Mitarbeitenden in der Verwaltung können durch den offenen Austausch von Daten schneller und präziser auf Herausforderungen reagieren.

## Prinzipien

Die Open Data Charter stellt einen internationalen Standard dar, entwickelt mit über 170 Regierungen und Organisationen weltweit, die sich für die Öffnung von Daten auf der Grundlage gemeinsamer Grundsätze einsetzen. Darin werden sechs Open Data Prinzipien als Normen für die Veröffentlichung von Daten beschrieben: [@OpenDataCharter]

1. Standardmäßig offen

Dies bedeutet einen echten Wandel in der Arbeitsweise der Behörden und in der Interaktion mit den Bürgern. Gegenwärtig müssen Datennutzer:innen Beamte oft um die gewünschten Informationen bitten. „Open by Default“ stellt dies auf den Kopf und besagt, dass eine Veröffentlichung für alle vorausgesetzt werden sollte. Die Regierungen müssen Daten, die unter Verschluss gehalten
werden, zum Beispiel aus Sicherheits- oder Datenschutzgründen, rechtfertigen. Damit dies funktioniert, müssen die Bürger auch darauf vertrauen können, dass offene Daten ihr Recht auf Privatsphäre nicht beeinträchtigen.

2. Rechtzeitig und umfassend

Offene Daten sind nur dann wertvoll, wenn sie noch relevant sind. Die rasche und umfassende Veröffentlichung von Informationen ist von
zentraler Bedeutung für ihren Erfolg. Die Regierungen sollten die Daten so weit wie möglich in ihrer ursprünglichen, unveränderten Form bereitstellen.

3. Zugänglich und maschinell nutzbar

Wenn sichergestellt wird, dass die Daten maschinenlesbar und leicht auffindbar sind, werden die Daten weiter verbreitet. Portale sind eine Möglichkeit, dies zu erreichen. Es ist aber auch wichtig, sich Gedanken über die Nutzererfahrung derjenigen zu machen, die auf die Daten zugreifen, einschließlich der Dateiformate, in denen die Informationen bereitgestellt werden. Die Daten sollten kostenlos und unter einer offenen Lizenz zur Verfügung gestellt werden, zum Beispiel unter der von Creative Commons entwickelten Lizenz.

4. Vergleichbar und interoperabel

Daten haben einen Multiplikatoreffekt. Je mehr qualitativ hochwertige Datensätze zur Verfügung stehen und je leichter sie miteinander kommunizieren können, desto größer ist der potenzielle Nutzen, den man aus ihnen ziehen kann. Gemeinsam vereinbarte Datenstandards spielen dabei eine entscheidende Rolle.

5. Verbesserte Governance und Bürgerbeteiligung

Offene Daten können den Bürgern (und anderen in der Verwaltung) eine bessere Vorstellung davon vermitteln, was Beamte und Politiker tun. Diese Transparenz kann die öffentlichen Dienste verbessern und dazu beitragen, dass die Regierungen zur Rechenschaft gezogen werden.

6. Für integrative Entwicklung und Innovation

Schließlich können offene Daten eine integrative wirtschaftliche Entwicklung vorantreiben. Ein besserer Zugang zu Daten kann zum Beispiel die Landwirtschaft effizienter machen oder zur Bekämpfung des Klimawandels eingesetzt werden. Schließlich denken wir bei offenen Daten oft nur an die Verbesserung der Leistung von Regierungen, aber es gibt ein ganzes Universum von Unternehmern, die mit offenen Daten Geld verdienen.

Die Open Data Strategie der Bundesregierung nennt diese Kriterien für Open
Data: [@Digitalstrategie]
- Behörden haben die Daten selbst erhoben oder durch Dritte erheben lassen
- frei über öffentliche Netze zugänglich
- maschinenlesbar
- nicht personenbezogen
- frei verwendbar
- keine sicherheitsrelevanten Informationen

## Umsetzung

Die Nutzung von Daten kann neben enormen Mehrwerten für Bürgerinnen und Bürger
sowie Wirtschaft und Wissenschaft auch für Staat und Verwaltung erhebliche
Mehrwerte generieren. Mit der rasant voranschreitenden Digitalisierung sind die
Verfügbarkeit von Daten und ihre Verknüpfung zu bedeutenden Faktoren für unsere
moderne Gesellschaft, Wirtschaft und Wissenschaft, aber auch für die tägliche
Arbeit von Politik und öffentlicher Verwaltung geworden.

Diese enorme Bedeutung wird durch die Veröffentlichung der Datenstrategie der
Bundesregierung besonders hervorgehoben. Dabei formuliert die Datenstrategie
vier Handlungsfelder: leistungsfähige und nachhaltig ausgestaltete
Dateninfrastrukturen, Steigerung innovativer und verantwortungsvoller
Datennutzung, Erhöhung der Datenkompetenz und Etablierung einer neuen
Datenkultur sowie die Rolle des Staates als Vorreiter der neuen Datenkultur [@Digitalstrategie].

Die Bundesregierung setzt Open Data in der Bundesverwaltung aktuell durch
folgende Maßnahmen um:

- Juli 2021: Die Open-Data-Strategie der Bundesregierung befasst sich neben ihren Zielen für Deutschland unter anderem mit Chancen und Mehrwerten von Open-Data, einer Bestandsaufnahme sowie einem Maßnahmenkatalog, der den Staat bei Open Data zum Vorreiter machen soll.

- Juni 2021: Verabschiedung Zweites Open Data Gesetz und Datennutzungsgesetz:
    - Ausweitung der Bereitstellung von Open Data auf nahezu die gesamte Bundesverwaltung
    - Bereitstellung von offenen Forschungsdaten
    - Schaffung verbindlicher Open-Data-Koordinatoren der Bundesbehörden
    - Datennutzungsgesetz zur Schaffung einheitlicher, nichtdiskriminierender Nutzungsbedingungen für Daten des öffentlichen Sektors – auch für öffentliche Unternehmen bestimmter Bereiche der Daseinsvorsorge
    - damit Umsetzung der 2019 neugefassten EU-Richtlinie 2019/1024 (Open-Data- und Public Sector Information-Directive) und Ablösung des Informationsweiterverwendungsgesetzes (IWG)

- Juni 2021: Verabschiedung des 3. Nationalen Aktionsplans (NAP) im Rahmen der Teilnahme Deutschlands an der Open Government Partnership (OGP), einem Zusammenschluss von inzwischen 78 Staaten, die sich für ein offenes Regierungshandeln einsetzen und verpflichten, im Geiste der „Open Government Declaration“ zu handeln. Das Ministerium ist mit der Maßnahme „Transparenz über Genehmigungsverfahren bei großen Infrastrukturvorhaben im Verkehrssektor“ im 3. NAP vertreten und führt die laufende Maßnahme „Open Data für intelligente Mobilität“ aus dem 1. NAP fort [@BMDV_OpenData].

In allen Bundesministerien sind zudem Datenlabore eingerichtet worden [@Netzpolitik_Datenlabore].

Diese Maßnahmen zeigen, dass die Bedeutung von Daten und offenen Daten in der Bundesverwaltung verstanden und verfestigt wird. In den Datenlaboren arbeiten Datenanalysten direkt in den Behörden.
Durch die Open Data Koordinatoren und die Datenlabore wird die verwaltungsinterne
Datenkompetenz gestärkt und eine eigenen Nachfrage etabliert. Diese Nachfrage
steigert Datenqualität und -quantität.

Die Durchführungsverordnung zur Festlegung von Hochwertigen Datensätzen zeigt, dass die EU den Wert von Daten erkannt hat. In der Verordnung werden homogene Daten und Meta-Daten gefordert. Die Daten sollen EU-weit bereitstehen. Dies wird zwangsläufig zu einer Bereitstellung gleicher Daten unter gleichen Datenformaten und gleichen Lizenzen auf allen Verwaltungsebenen führen [@GOVDATA_HochwertigeDatensätze].

## Kritik im Zusammenhang mit Open Data

Trotz der positiven Aspekte von Open Data gibt es auch Kritikpunkte. Datenschutz und Privatsphäre sind zentrale Anliegen, da durch die Offenlegung von Daten die Gefahr besteht, dass persönliche Informationen missbräuchlich genutzt werden. Ein sensibler Umgang mit offenen Daten ist daher von großer Bedeutung.

Insbesondere Datenbereitsteller sehen oft Bedenken hinsichtlich möglicher Missinterpretationen und Sicherheitsrisiken. Es ist wichtig, klare Leitlinien und Schulungen bereitzustellen, um den verantwortungsbewussten Umgang mit offenen Daten zu gewährleisten. Dabei sollten Datenschutzmaßnahmen und Sicherheitsvorkehrungen kontinuierlich evaluiert und verbessert werden.
Oft wird zudem auf Kosten und Aufwände in der Bereitstellung von Open Data verwiesen. Hierbei bleibt oft unberücksichtigt, dass die automatisierte Veröffentlichung von Daten auch die Zugänglichkeit von Daten innerhalb der Verwaltung erheblich erhöht (verbessertes Datenmanagement). Zudem kann durch die Umsetzung von Open-Data-Vorgaben auch das Wissen über den Umgang mit Daten (Data Literacy / Datenkompetenz) in der Verwaltung erheblich gesteigert werden. Bisher manuelle durchgeführte Verwaltungsprozesse können so zukünftig (teil-)automatisiert werden. Die Kritik in diesem Zusammenhang wird häufig mit den Kosten und der Umsetzungszeit von Digitalisierung der Verwaltung verwechselt.

Zudem wird die Zugänglichkeit von Open Data aufgrund verschiedener technischer Barrieren in Frage gestellt. Dabei bleibt unberücksichtigt, dass auch Menschen ohne Daten- und IT-Kompetenz von offenen Daten profitieren, wenn diese durch Dritte (Wissenschaft, Datenjournalismus, Zivilgesellschaft, ...) aufbereitet und zum Beispiel in Form von Analysen oder interaktiven Tools für alle zugänglich gemacht werden.
Insgesamt bieten offene Verwaltungsdaten die Möglichkeit, die Beziehung
zwischen Bürgern und Verwaltung zu stärken. Transparenz fördert das Vertrauen,
während Partizipation die Demokratie belebt. Die Kritikpunkte dürfen dabei
nicht ignoriert werden, sondern sollten Anlass für eine verantwortungsbewusste
Gestaltung und Nutzung von offenen Daten sein.

## Quellen

