---
sidebar_position: 1
description: Das Warum und Wie der Dokumentation
---

# Kompass der föderalen IT-Architektur

Der Erfolg von Digitalisierungsprojekten im öffentlichen Sektor wird in der Regel aus der Perspektive von Verwaltungskund:innen bewertet: Je „digitalisierter“ der Zugang zu Verwaltungsleistungen, desto erfolgreicher das Projekt. Die Umsetzung des Onlinezugangsgesetzes ist ein aktuelles und passendes Beispiel. Ein Blick in das [OZG-Dashboard](https://dashboard.ozg-umsetzung.de/) verdeutlicht den starken Fokus auf die Möglichkeit, Anträge online zu stellen. Wie sich im späteren Verlauf zeigen wird, ist dies aus Verwaltungswissenschaftlicher Sicht ein wichtiger und richtiger Ansatz.

Doch ein digitalisierter Verwaltungszugang ist nur ein Schritt für eine nachhaltig funktionierende Digitale öffentliche Verwaltung im Sinne einer Ende-zu-Ende (E2E) Digitalisierung. Diese umfasst erstens auch eine Auseinandersetzung mit den Geschäftsprozessen, die den einzelnen Leistungen zugrunde liegen. Hierbei ist sowohl eine strukturelle, nämlich gesetzeskonforme, als auch eine praktische, die Arbeitsebene in den einzelnen Organisationen betreffende Sicht notwendig. Zweitens sollen die vorhandenen technischen Infrastrukturen in den einzelnen Behörden, auf Landes- und auf Bundesdeutscher Ebene in die Konzeption mit einbezogen werden. Drittens müssen auch die Fragen nach vorgegebenen/empfohlenen technischen Standards, Daten- und IT-Sicherheit im Allgemeinen beantwortet werden. Zusammengefasst: Für einen zukunftsfähigen E2E-digitalisierten öffentlicher Sektor bedarf es unbedingt einer holistischen Konzeption und Umsetzung.

## Warum der Kompass

Anstelle des beobachtbaren Nebeneinanders von Expert:innenwissen und darauf aufbauenden Einzellösungen, plädiert die Forschungsgruppe des Projekts [simPLEX](https://www.opendva.de) für ein offen zugängliches, vernetztes Fachdomänenwissen und die Integrationen verschiedener Sichtweisen in den Diskurs über eine vernetzte bundesdeutsche Verwaltungsdigitalisierung.

Aus dieser Motivation ist die hier vorliegende Dokumentation entstanden. In Zusammenarbeit mit Projektpartnern aus der kommunalen Verwaltung Jena, der FITKO, des Deutschen Instituts für Luft- und Raumfahrttechnik (DLR) und dem Stein-Hardenberg-Institut (SHI) wurde die Basis für eine umfassende Wissenssammlung über die Funktionsweise des öffentlichen Sektors, der bestehenden bundesdeutschen IT-Landschaft und das Zielbild *Ende-zu-Ende-digitalisierte öffentliche Verwaltung* geschaffen.

## Wie er funktioniert / Ziel

Der Kompass soll kleine IT-Firmen dazu befähigen, an diesem Digitalisierungsvorhaben teilzunehmen. Er soll als Nachschlagewerk für Entwickler:innen, Mitarbeitende von öffentlichen Verwaltungen und alle Interessierten dienen. Außerdem soll diese Dokumentation weiterleben: Dafür hat die Projektgruppe einen kollaborativen Ansatz gewählt, sodass jede:r die Inhalte ergänzen und kommentieren kann. 
Pointiert formuliert: Der Kompass soll als eine Plattform für den Wissensaustausch verschiedener Fachdomänen und somit für die Vernetzung dieser fungieren.

## Aufbau

Die Beiträge der Dokumentation beziehen sich zwar vereinzelt aufeinander, bauen jedoch nicht zwingend in chronologischer Folge aufeinander auf. Deshalb können die einzelnen Kapitel auch unabhängig voneinander gelesen werden.
Das einleitende Kapitel führt zunächst in die grundsätzlichen Funktionsweisen öffentlicher Verwaltungen und diesen zugrundeliegenden Prinzipien ein.
Darauf folgt eine Sammlung der zentralen Leitbilder für eine digitalisierte Verwaltung: Darunter das Konzept von Open Data, das antragsfreie Verfahren oder ein Analyseraster für die Nachvollziehbarkeit des Verwaltungszugangs.
In einem nächsten Schritt werden die Rahmenbedingungen für Digitalisierung kurz erläutert: OZG, SDG-VO, FIM und die beteiligten Akteure auf den verschiedenen Ebenen.
Kernstück des Kompasses ist eine vertiefte Analyse der bisher entwickelten und geplanten technischen Komponenten, gegliedert nach den Schichten der Bundesarchitektur der FITKO: Plattformen/Portale, Fachanwendungen, Querschnittskomponenten und Datenhaltung.
Abschließend erfolgt eine kritische Einordnung der Analyse.
