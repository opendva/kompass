# Kompass der föderalen IT-Architektur

In diesem Repository werden der Quelltext und die Inhalte verwaltet, welche zum Erstellen der Seiten hinter https://docs.fitko.de/kompass verwendet werden. Hierbei kommt ein *Static-Website Generator* namens [*Docusaurus 2*](https://docusaurus.io/) zum Einsatz. Dieser ermöglicht das Erfassen von Inhalten in einfachen *Markdown*-Dateien, bietet außerdem die Möglichkeit, den vollen Umfang des [*React-Frameworks*](https://react.dev/) zu nutzen und ist darüber hinaus sehr flexibel konfigurier- und erweiterbar.

Der *Kompass der föderalen IT-Architektur* lebt von der Zusammenarbeit. Nur durch eine starke Community können die Inhalte aktuell gehalten und weiter ausgebaut werden.  
Wenn Du also etwas beitragen kannst, dann zögere nicht und unterstütze uns beim Aufbau der größten Wissenssammlung über
* die Funktionsweise des öffentlichen Sektors,
* die bestehende bundesdeutsche IT-Landschaft und
* das Zielbild *Ende-zu-Ende-Digitalisierung* der öffentlichen Verwaltung.

Registriere Dich hierzu bei [Open CoDE](https://opencode.de/de/registrieren) und folge den unten stehenden *Hinweisen zur Mitarbeit*.  
Für Fragen, Probleme, Anregungen und Ideen nutze bitte die [Issues](https://gitlab.opencode.de/opendva/kompass/-/issues) dieses Repositories.

# Hinweise zur Mitarbeit
## Workflow
Der einfachste Weg zur Mitarbeit ist die Erstellung eines [Forks](https://gitlab.opencode.de/opendva/kompass/-/forks/new). So kannst du in einer eigenen Version dieses Repositories sofort loslegen, deine Änderungen machen und sie abschließend durch einen Merge Request hier einfügen.  
Sollte sich eine längerfristige Mitarbeit abzeichnen, empfiehlt es sich direkten Zugriff zum Projekt zu [beantragen](https://gitlab.opencode.de/opendva/kompass/-/project_members/request_access).

## Branches, Commits und Merge Requests
Der `main`-Branch korrespondiert immer zum aktuellen Stand von https://docs.fitko.de/kompass.  
Der erste Schritt zur Mitarbeit ist daher immer die Erstellung eines eigenen (Arbeits-)Branches. (entfällt beim Forken)  
Darin können nach Belieben Änderungen gemacht und in Commits festgehalten werden. Commit-Messages sollten bevorzugt in englischer Sprache verfasst werden und im Imperativ, Präsens stehen. D.h. sie vervollständigen den Satz *“If applied, this commit will …”* (Bsp.: *`Add content to example.md`; `Fix dead links in example.md`; `Create new folder named 'example'`*). Außerdem sollten sie kurz und prägnant sein. Dementsprechend sollten auch die Änderungen innerhalb eines Commits nicht umfänglicher sein, als in einer kurzen Commit-Message beschrieben werden kann.  
Um die gemachten Änderungen aus dem eigenen Branch in den `main`-Branch zu überführen, muss ein entsprechender Merge Request erstellt werden.

## Wo finde ich die Inhalte?
Alle Inhalte befinden sich in Form von *Markdown*-Dateien innerhalb des Ordners `docs`. Diese Dateien Enden auf `.md`. Daneben gibt es noch Dateien mit der Endung `.mdx`. Diese nutzen [*MDX*](https://mdxjs.com), eine Erweiterung von *Markdown*, um *JSX*-Komponenten (in diesem Fall *React*-Komponenten) in *Markdown* einzubetten. Weitere Informationen zu *Markdown* im Allgemeinen und eine Übersicht über die wichtigsten Syntaxelemente (*Cheat Sheet*) finden sich [hier](https://www.markdownguide.org/getting-started/).

## Wie kann ich Bilder hinzufügen und in den Text einbinden?
Neben der Einbindung von Online-Ressourcen können Medien auch als Datei im Repository abgelegt und innerhalb der *Markdown*-Dateien verwendet werden. Der Ort für diese ist immer der entsprechende Ordner für den jeweiligen Abschnitt unter `static/assets/`. Die Einbindung erfolgt bspw. so: 
```markdown
![Architektur des DVDV](/assets/IT-Landschaft/architektur_dvdv.png)
```

## Wie kann ich Quellen zitieren?
Gute Quellenarbeit ist essentiell für die Bereitstellung von validen Informationen. Wir möchten daher jeden bitten, die hinzugefügten Inhalte nach bestem Wissen und Gewissen mit Literaturnachweisen zu belegen.  
Hierzu haben wir ein Plugin namens [*rehype-citation*](https://github.com/timlrx/rehype-citation) in diese *Docusaurus*-Instanz integriert. Dieses macht es möglich, [*BibTeX*](http://www.bibtex.org/de/) als System zum Verwalten und Referenzieren von Quellennachweisen zu nutzen. Kern dieses Systems ist eine Bibliografie-Datei, in unserem Falle `references.bib`, in welcher alle Quellen gesammelt und in einer genau definierten Struktur notiert werden.  
Ein Blick in diese Datei verrät vermutlich mehr als tausend Worte, dennoch folgt eine kurze Erklärung am Beispiel:

```tex
@Book{osterloh-frost,
  author    = {Osterloh, Margit; Frost, Jetta},
  title     = {Prozessmanagement als Kernkompetenz},
  publisher = {Gabler Verlag},
  year      = {2006}
}
```
* `@Book` - bezeichnet den Publikations-Typ; andere Beispiele sind `@Article`, `@Incollection` oder `@Online`
* `osterloh-frost` - Schlüsselwort des Eintrags; wird später im Text verwendet um diese Quelle zu referenzieren; muss daher einzigartig unter allen Einträgen sein
* `author ... year` - Tags; Datenfelder zur Quelle

(Literaturdatenbanken bzw. -suchmaschinen wie etwa *Google Scholar* verfügen meist über die Möglichkeit die Quelleninformationen direkt im *BibTeX*-Format zu exportieren.)

Möchte man also einen Literaturverweis im Text erstellen, fügt man zunächst der `references.bib` den entsprechenden Eintrag hinzu, sofern dieser noch nicht vorhanden ist, und kann anschließend innerhalb der `.md`-Datei das Schlüsselwort verwenden, um die Referenz zu setzen. Die Syntax hierfür ist `[@Schlüsselwort]`. An unserem Beispiel:
```markdown
Lorem [@osterloh-frost] ipsum ...
```
Auf der durch *Docusaurus* erstellten Website werden die Referenzen schließlich durch Nummern ersetzt und am Ende der Seite werden alle Quellen im Literaturverzeichnis aufgelistet.  
Soll das Literaturverzeichnis an einer anderen Stelle auf der Seite eingefügt werden, kann man `[^ref]` benutzen, um die gewünschte Stelle anzuzeigen.  
**ACHTUNG:** Die Überschrift `## Quellen` wird vom Plugin **nicht** automatisch hinzugefügt und muss daher immer händisch am Ende der Seite (oder direkt über `[^ref]`) eingefügt werden.

## Was ist beim Anlegen von Dateien und Ordnern zu beachten?
Die Datei- und Ordnernamen innerhalb des `docs`-Ordners werden von *Docusaurus* benutzt, um die URL zusammenzusetzen, unter welcher die resultierende Seite schließlich erreichbar sein wird.  
Um eine gewisse Uniformität zu wahren, halte Dich beim Anlegen von neuen Dateien und Ordnern bitte an folgende Namenskonvention:
* Verwende nur Kleinbuchstaben und Bindestriche.
* Verwende keine Umlaute.
* Der Name sollte auf den Inhalt schließen lassen, **aber**
* so kurz wie möglich sein.

Beispiel:
```
docs/verwaltung-von-morgen/antragslose-verfahren.md
```
ist erreichbar unter
```
https://docs.fitko.de/kompass/docs/verwaltung-von-morgen/antragslose-verfahren
```

## Woher kommt die *Sidebar*?
Die Navigationsleiste an der linken Seite, *Sidebar* genannt, wird von *Docusaurus* anhand der Ordnerstruktur unter `docs` und der darin enthaltenen *Markdown*-Dateien automatisch generiert. Hierbei werden die Ordnernamen und jeweils die erste `# Überschrift` innerhalb einer *Markdown*-Datei als Bezeichnung der *Sidebar*-Elemente verwendet. Die Sortierung erfolgt alphabetisch.  
Dieses Standardverhalten kann aber manipuliert werden. In der offiziellen Dokumentation finden sich dazu ausführliche [Informationen](https://docusaurus.io/docs/sidebar/autogenerated). In unserem Falle nutzen wir davon typischer Weise folgende Funktionen:
* *Front Matter* innerhalb von *Markdown*-Dateien zur Manipulation der anklickbaren Elemente
  ```
  ---
  sidebar_position: 1
  sidebar_label: Antragslose Verfahren
  description: das antragslose Verfahren ist die Idealvorstellung von digitaler Verwaltung.
  ---
  ```
* `_category_.json` innerhalb eines Ordners zur Manipulation der aufklappbaren Elemente
  ```json
  {
    "position": 2,
    "label": "Ideen und Gedankenansätze aus der Verwaltung von Morgen",
    "link": {
        "type": "generated-index",
        "description":
          "Die Strategien, Projekte und Produkte zur Digitalisierung [...] kurz beschrieben."
    }
  }
  ```
Hierbei ist zu erwähnen, dass `sidebar_position` und `position` nicht nur ganzzahlig sein können. Möchte man ein Element zwischen `1` und `2` anordnen kann hierfür bspw. `1.5` als Position angegeben werden, wodurch das Bearbeiten aller folgenden Elemente unnötig wird.

## Wie kann ich meine Änderungen testen?
Bevor Änderungen in den `main`-Branch übernommen werden, sollte unbedingt getestet werden, ob die Erstellung der betroffenen Webseiten wie erwartet funktioniert. Hierzu gibt es prinzipiell zwei Möglichkeiten:

## Wie kann ich Diagramme erstellen?

Diagramme können mit [Mermaid.js](https://mermaid.js.org/intro/) direkt in der Dokumentation erstellt werden. Dazu legt man einfach einen Code Block mit `mermaid` als Sprache an:
````
```mermaid
  sequenceDiagram

  # Participants
  participant A as Alice
  participant B as Bob

  # Relations
  A ->> B: Hi Bob!
  B ->> A: Nice to meet you, Alice
```
````

### Lokales Deployment
Wer in einer lokalen Kopie dieses Repositories auf seinem Rechner arbeitet und [*Yarn*](https://classic.yarnpkg.com/en/docs/install) installiert hat, kann den *Docusaurus Development Server* starten. Dieser erzeugt die Webseiten und stellt sie über einen Webserver unter http://localhost:3000 zur Verfügung. Der große Vorteil hierbei ist, dass alle Änderungen sofort reflektiert werden ohne den Server neustarten zu müssen. Somit sind etwaige Fehlerdiagnosen einfacher durchzuführen und bereits vor dem Commit möglich.
Man geht dazu folgender Maßen vor: Man öffnet eine Konsole innerhalb des Repositories und installiert mit dem Befehl
```console
yarn
```
die notwendigen Packages. Dies ist ein einmaliger Vorgang. Anschließend kann der *Development Server* mit
```console
yarn start
```
gestartet werden. Zum Stoppen verwendet man `Ctrl+C`.

### *Review Deployment*
Mit dem Erstellen eines jeden Merge Requests wird automatisch eine *CI/CD Pipeline* gestartet, welche ein sogenanntes *Review Deployment* erzeugt. Dieses ist direkt über die Übersichtsseite des Merge Requests erreichbar. Dort sollte nach erfolgreichem Durchlaufen der Pipeline ein *View app*-Button angezeigt werden. (Falls dies nicht der Fall ist, kann ein Neuladen der Seite helfen.)  
Wer also nicht lokal arbeitet, sondern den Online-Editor von *GitLab* nutzt, kann Fehler erst nach dem Stellen des Merge Requests finden und muss diesen für eventuelle Korrekturen um weitere Commits ergänzen.

# Lokale Apache-Testumgebung für Docusaurus

Diese Docker-Konfiguration ermöglicht das lokale Testen von Docusaurus-Builds inklusive .htaccess-Regeln.

## Voraussetzungen

- Node.js
- Optional: Yarn Package manager (npm kann auch verwendet werden)
- Docker & Docker Compose (beides in Docker Desktop enthalten)
- Docusaurus Build im `build/` Verzeichnis
- .htaccess-Datei im `static/` Verzeichnis

## Schnellstart

1. Build der Docusaurus-Seite erstellen:
```bash
$ yarn install
$ yarn build
```

2. Docker-Container starten:
```bash
$ docker-compose up --build
```

Die Seite ist dann unter `http://localhost:8090` erreichbar.

## Projektstruktur

```
.
├── build/             # Docusaurus Build-Output
├── static/
│   └── .htaccess      # Apache-Konfiguration
├── docker-compose.yml
├── Dockerfile
└── my-httpd.conf      # Apache-Hauptkonfiguration
```

## Entwicklung

- Änderungen an der `.htaccess` und am am Build-Output werden durch das Volume-Mounting sofort wirksam

## Steuerung

```bash
# Container im Vordergrund starten
docker-compose up --build

# Container im Hintergrund starten
docker-compose up -d --build

# Container stoppen
docker-compose down
```
