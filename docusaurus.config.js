/** @type {import('@docusaurus/types').DocusaurusConfig} */

const projectName = 'Kompass der föderalen IT-Architektur'

const baseUrl = process.env.DOCUSAURUS_BASE_URL || '/kompass/'
const gitBranch = process.env.GIT_BRANCH || 'main'
const gitlabUrl = process.env.GITLAB_SERVER_URL || 'https://gitlab.opencode.de'
const gitlabProjectPath = process.env.GITLAB_PROJECT_PATH

async function createConfig() {
  const rehypeCitation = (await import('rehype-citation')).default;
  return {
    title: projectName,
    url: 'https://docs.fitko.de',
    baseUrl,
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'throw',
    favicon: 'favicon.png',
    customFields: {
      feedbackUrl: `${gitlabUrl}/${gitlabProjectPath}/-/issues`,
      sourceCodeUrl: `${gitlabUrl}/-/ide/project/${gitlabProjectPath}`,
    },
    i18n: {
      defaultLocale: 'de',
      locales: ['de'],
    },
    themes: ['@docusaurus/theme-mermaid'],
    markdown: {
      mermaid: true,
    },
    themeConfig: {
      docs: {
        sidebar: {
          hideable: true,
        },
      },
      mermaid: {
        options: {
          startOnLoad: true,
          sequence: { showSequenceNumbers: true },
          theme: 'base',
          themeVariables: {
            textColor: 'rgb(0,0,0)',
            primaryBorderColor: 'rgb(87,87,87)',
            lineColor: 'rgb(87,87,87)',
            mainBkg: 'rgb(255,200,25)',
            sequenceNumberColor: 'rgb(255,255,255)',
          },
        },
      },
      colorMode: {
        disableSwitch: true,
      },
      navbar: {
        title: projectName,
      },
      footer: {
        style: 'light',
        copyright: `Diese Seite ist Teil des <a href="https://docs.fitko.de/">Föderalen Entwicklungsportals</a>. Verantwortlich für die Inhalte der Seite sind die jeweiligen Autoren. Wenn nicht anders vermerkt, sind die Inhalte dieser Webseite lizenziert unter der <a href="https://creativecommons.org/licenses/by/4.0/deed.de">Creative Commons Namensnennung 4.0 International Public License (CC BY 4.0)</a>. Die technische Infrastruktur wird betrieben durch die FITKO. Es gilt das <a href="https://www.fitko.de/impressum">Impressum der FITKO</a> und die <a href="https://fitko.de/datenschutz">Datenschutzerklärung der FITKO</a> mit der Maßgabe, dass kein Tracking durchgeführt wird und keine Cookies gesetzt werden.`,
      },
    },
    plugins: [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      // '@docusaurus-terminology/parser',
      'docusaurus-plugin-sass',
      async function myPlugin(context, options) {
        return {
          name: "docusaurus-tailwindcss",
          configurePostCss(postcssOptions) {
            // Appends TailwindCSS and AutoPrefixer.
            postcssOptions.plugins.push(require("tailwindcss"));
            postcssOptions.plugins.push(require("autoprefixer"));
            return postcssOptions;
          },
        };
      },
    ],
    presets: [
      [
        '@docusaurus/preset-classic',
        {
          docs: {
            sidebarPath: require.resolve('./sidebar.js'),
            editUrl: ({ version, versionDocsDirPath, docPath }) =>
              `https://gitlab.opencode.de/opendva/kompass/edit/${gitBranch}/${versionDocsDirPath}/${docPath}`,
            routeBasePath: 'docs',
            breadcrumbs: false,
            showLastUpdateTime: true,
            rehypePlugins: [
              [rehypeCitation,
                {
                  path: "./citation_config",
                  bibliography: "references.bib",
                  csl: "din-1505-2-numeric.csl",
                  lang: "csl-de-DE.xml",
                  linkCitations: true
                }],
            ],
          },
          theme: {
            customCss: require.resolve('./src/css/custom.scss'),
          },
        },
      ],
    ],
  };
}

module.exports = createConfig;
